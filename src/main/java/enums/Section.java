package enums;

public enum Section {
    LOGIN,
    TERMINAL,
    INVENTORY,
    SETTINGS,
    EXIT,
    STORE,
    STATUS,
    PLAY;
}
