package enums;

import hero.HeroPower;
import hero.SpecialPower;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public enum Hero {

    MAGE("Mage", new ArrayList<>(Arrays.asList("Arcane Intellect", "Polymorph")),
            new SpecialPower("Spells cost 2 less mana"),
            new HeroPower("Fireblast","Deal 1 damage",2)),
    ROGUE("Rogue", new ArrayList<>(Arrays.asList("Friendly Smith", "Soul Stealer")),
            new SpecialPower("Cards from other heroes cost 2 less mana"),
            new HeroPower("CardStealer","Steals a card from opponent",3)),
    WARLOCK("Warlock", new ArrayList<>(Arrays.asList("Dreadscale", "Fear God")),
            new SpecialPower("This hero starts with 35 health"),
            new HeroPower("Helper","Grant +1/+1 to a random Minion or adds a card to hand",2)),
    HUNTER("Hunter", new ArrayList<>(Collections.singletonList("Swamp King Dred")),
            new SpecialPower("All Minions have rush ability"),
            new HeroPower("Caltrops","After your opponent plays a minion, deal 1 damage to it",0)),
    PALADIN("Paladin", new ArrayList<>(Collections.singletonList("Gnomish Army Knife")),
            new SpecialPower("After each turn, grants all minions +1/+1"),
            new HeroPower("The Silver Hand","Summon two 1/1 Recruits",2)),
    PRIEST("Priest", new ArrayList<>(Collections.singletonList("High Priest Amet")),
            new SpecialPower("Each healing spell effect twice as normal"),
            new HeroPower("Heal","Restore 4 Health",2)),
    NATURAL("Natural", new ArrayList<>(Arrays.asList("Goldshire Footman",
            "Arcane Explosion", "Magma Rager", "Stonetusk Boar", "Starfire", "Nightblade")), null, null);


    public static final Hero DEFAULT_HERO = MAGE;
    private final String name;
    private ArrayList<String> defaultCards;
    SpecialPower specialPower;
    HeroPower heroPower;


    Hero(String name, ArrayList<String> defaultCards, SpecialPower specialPower, HeroPower heroPower) {
        this.name = name;
        this.defaultCards = defaultCards;
        this.specialPower = specialPower;
        this.heroPower = heroPower;
    }

    public static Hero getByName(String heroName) {
        for (Hero hero : getAllHeroes()) {
            if (hero.getName().toLowerCase().equals(heroName.toLowerCase())) {
                return hero;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public static ArrayList<Hero> getAllHeroes() {
        return new ArrayList<>(Arrays.asList(Hero.values()));
    }

    public ArrayList<String> getDefaultCards() {
        return defaultCards;
    }

    public SpecialPower getSpecialPower() {
        return specialPower;
    }

    public HeroPower getHeroPower() {
        return heroPower;
    }
}
