package enums;

public enum Rarity {
    FREE(0),
    COMMON(10),
    UNCOMMON(50),
    RARE(100),
    EPIC(200),
    LEGENDARY(500);

    private int value;

    Rarity(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int greaterThan(Rarity rarity) {
        return Integer.compare(this.value, rarity.value);
    }
}
