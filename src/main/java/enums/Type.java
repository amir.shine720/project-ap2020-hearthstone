package enums;

public enum Type {
    SPELL,
    MINION,
    PASSIVE,
    WEAPON,
    QUEST
}
