package enums;

public enum LogEvent {
    SIGNIN("sign_in"),
    NAVIGATE("navigate"),
    LIST("list"),
    SELECT("select"),
    ADD("add"),
    REMOVE("remove"),
    DELETE_ACCOUNT("delete_account"),
    SHOW("show"),
    BUY("buy"),
    SELL("sell"),
    LOGOUT("logout"),
    ERROR("error"),
    CLICK_BUTTON("click_button"),
    CLICK_CARD("click_card"),
    ABANDON("abandon"),
    CLICK_DECK("click_deck"),
    CREATE_DECK("create_deck");

    private final String text;

    LogEvent(String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }
}
