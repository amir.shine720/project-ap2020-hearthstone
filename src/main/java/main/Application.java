package main;

import ui.MainFrame;

import javax.swing.*;

public class Application {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(MainFrame::new);
    }
}

