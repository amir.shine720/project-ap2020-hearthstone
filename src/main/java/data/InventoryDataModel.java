package data;

import utils.ImageGetter;

import javax.swing.*;

public class InventoryDataModel {
    public ImageIcon getSearchIcon(){
        return ImageGetter.createImageIcon("src/main/resources/icons/search_icon.jpg","searchIcon");
    }

    public ImageIcon getManaIcon() {
        return ImageGetter.createImageIcon("src/main/resources/icons/mana_icon.png", "manaIcon");
    }
}
