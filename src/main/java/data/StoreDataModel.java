package data;

import cards.Card;
import enums.Hero;

import java.util.ArrayList;

public class StoreDataModel {

    public ArrayList<Card> getHeroCards() {
        return CardProvider.getHeroCards(Player.getInstance().getHero());
    }

    public ArrayList<Card> getHeroCards(Hero hero) {
        return CardProvider.getHeroCards(hero);
    }
}
