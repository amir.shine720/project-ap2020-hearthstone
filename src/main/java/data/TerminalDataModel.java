package data;

import cards.Card;

import java.util.List;

public class TerminalDataModel {
    public List<Card> getAllCards() {
        return CardProvider.getAllCards();
    }
}