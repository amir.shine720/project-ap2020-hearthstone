package data;

import cards.Card;
import com.google.gson.Gson;
import enums.Hero;
import utils.ImageGetter;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class PlayDataModel {

    public ArrayList<Card> getPassiveCards() throws IOException {
        return getFromFile(new File("src/main/resources/cards/passive"));
    }

    private ArrayList<Card> getFromFile(File file) throws IOException {
        ArrayList<Card> cards = new ArrayList<>();
        Gson gson = new Gson();
        for (File f : Objects.requireNonNull(file.listFiles())) {
            try (BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath()))) {
                cards.add(gson.fromJson(br, Card.class));
            }
        }
        return cards;
    }

    public ImageIcon getHeroImage(Hero hero) {
        return ImageGetter.getImage(String.format("src/main/resources/icons/%s_image.png", hero.getName().toLowerCase()),
                hero.getName() + " Image");
    }
}
