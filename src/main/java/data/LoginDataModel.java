package data;

import com.google.gson.Gson;
import enums.LogEvent;
import logger.MyLogger;

import java.io.*;

public class LoginDataModel {
    public static Player getPlayerData(String username) {
        Gson g = new Gson();
        try (BufferedReader br = new BufferedReader(new FileReader(String.format("./src/main/resources/players/%s.json", username)))){
            Player player = g.fromJson(br, Player.class);
            br.close();
            return player;
        } catch (NullPointerException | IOException e) {
            return null;
        }
    }

    public boolean addPlayer() throws IOException {
        String path = String.format("src/main/resources/players/%s.json", Player.getInstance().username);
        if (new File(path).exists()) {
            return false;
        }
        savePlayerData();
        MyLogger.addPlayerLog();
        return true;
    }

    public void savePlayerData() throws IOException {
        Gson gson = new Gson();
        String json = gson.toJson(Player.getInstance());
        String path = String.format("src/main/resources/players/%s.json", Player.getInstance().username);
        try (FileWriter writer = new FileWriter(path)) {
            writer.write(json);
        }
    }
}
