package data;

import cards.Card;
import cards.Deck;
import enums.Hero;
import utils.FullDeckException;
import utils.Password;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Player {
    String username;
    String password;
    ArrayList<Card> cards = new ArrayList<>();

    ArrayList<Deck> decks = new ArrayList<>();
    Deck currentDeck;

    private static Player instance;
    private Hero hero;
    private int coins;
    private String id;

    private Player() {
    }

    public static Player getInstance() {
        if (instance == null) {
            instance = new Player();
        }
        return instance;
    }

    public void initialize(Player playerData) {
        Player.getInstance().initialize(playerData.getUsername(), playerData.password, playerData.decks,
                playerData.getCards(), playerData.hero, playerData.coins, playerData.getID());
    }

    public void initialize(String username, String password, ArrayList<Deck> defaultDecks, ArrayList<Card> cards, Hero defaultHero, int coins,
                           String id) {
        this.username = username;
        this.password = password;
        setCards(cards);
        setDecks(defaultDecks);
        setCurrentDeck(getDecks().get(0));
        setHero(defaultHero);
        setCoins(coins);
        if (id == null) {
            setID(UUID.randomUUID().toString());
        } else {
            setID(id);
        }
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCurrentDeck(Deck currentDeck) {
        this.currentDeck = currentDeck;
    }

    public ArrayList<Deck> getDecks() {
        return decks;
    }

    public String getPassword() {
        return Password.decrypt(password);
    }

    public void clear() {
        instance = null;
    }

    public ArrayList<Card> getCurrentHeroCards() {
        ArrayList<Card> currentHeroCards = new ArrayList<>();
        for (Card card : cards) {
            if (card.getHero() == getHero() || card.getHero() == Hero.NATURAL) currentHeroCards.add(card);
        }
        return currentHeroCards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public Hero getHero() {
        return this.hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public Deck getCurrentDeck() {
        return currentDeck;
    }

    public void setDecks(ArrayList<Deck> decks) {
        this.decks = decks;
    }

    public void addCardToDeck(Card card) throws FullDeckException {
        if (currentDeck.getCards().size() == 15) {
            throw new FullDeckException();
        }
        currentDeck.addCardToDeck(card);
    }

    public void removeCardFromCurrentDeck(Card card) {
        currentDeck.removeCardFromDeck(card);
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public void removeCard(Card card) {
        cards.remove(card);
        for (Deck deck : getDecks()) {
            if (deck.getCards().contains(card)) {
                removeCardFromAllDecks(card);
            }
        }
    }

    private void removeCardFromAllDecks(Card card) {
        for (Deck deck : getDecks()) {
            if (deck.getCards().contains(card)) {
                deck.removeCardFromDeck(card);
            }
        }
    }

    public String getUsername() {
        return username;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public void reduceCoins(int amount) {
        coins -= amount;
    }

    public void increaseCoins(int amount) {
        coins += amount;
    }

    public String getEncryptedPassword() {
        return password;
    }

    public void addDeck(Deck deck) {
        decks.add(deck);
    }
}
