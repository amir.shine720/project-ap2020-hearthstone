package data;

import cards.Card;
import cards.Deck;
import com.google.gson.Gson;
import enums.Hero;
import enums.LogEvent;
import logger.MyLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CardProvider {
    private static HashMap<Hero, ArrayList<Card>> cards = new HashMap<>();
    private static ArrayList<Card> passiveCards = new ArrayList<>();

    public static void initializeCards() {
        for (Hero hero : Hero.getAllHeroes()) {
            File heroFiles = new File(
                    String.format("src/main/resources/cards/%s", hero.toString().toLowerCase()));

            ArrayList<Card> cardsArray = new ArrayList<>();
            try {
                getFromFile(heroFiles, cardsArray);
//                setPassiveCards(cardsArray);
            } catch (IOException e) {
                MyLogger.log(LogEvent.ERROR, e.getMessage());
            }
            cards.put(hero, cardsArray);
        }

        try {
            setPassiveCards(passiveCards);
        } catch (IOException e) {
            MyLogger.log(LogEvent.ERROR, e.getMessage());
        }

    }

    private static void setPassiveCards(ArrayList<Card> passiveCards) throws IOException {
        getFromFile(new File("src/main/resources/cards/passive"), passiveCards);

    }

    public static ArrayList<Card> getPassiveCards() {
        return passiveCards;
    }

    private CardProvider() {}

    public static ArrayList<Card> getHeroCards(Hero hero) {
        return cards.get(hero);
    }

    public static List<Card> getAllCards() {
        ArrayList<Card> allCards = new ArrayList<>();
        for (Hero hero : Hero.getAllHeroes()) {
            allCards.addAll(cards.get(hero));
        }
        return allCards;
    }

    public static ArrayList<Deck> getDefaultDecks() {
        ArrayList<Deck> defaultDecks = new ArrayList<>();
        for (Hero hero : Hero.getAllHeroes()) {
            if (hero == Hero.NATURAL) {
                continue;
            }
            ArrayList<Card> defaultCards = new ArrayList<>();
            for (Card card : getHeroCards(hero)) {
                if (hero.getDefaultCards().contains(card.getName())) {
                    defaultCards.add(card);
                }
            }
            for (Card card : getHeroCards(Hero.NATURAL)) {
                if (Hero.NATURAL.getDefaultCards().contains(card.getName())) {
                    defaultCards.add(card);
                }
            }

            Deck deck = new Deck(String.format("%sDefaultDeck", hero.getName()), hero, defaultCards);

            defaultDecks.add(deck);
        }
        return defaultDecks;
    }

    public static ArrayList<Card> getDefaultCards() {
        ArrayList<Card> cardArrayList = new ArrayList<>();
        for (Deck i : getDefaultDecks()) {
            for (Card card : i.getCards()) {
                if (!cardArrayList.contains(card)) {
                    cardArrayList.add(card);
                }
            }
        }
        return cardArrayList;
    }

    private static void getFromFile(File hero, ArrayList<Card> cards) throws IOException {
        Gson gson = new Gson();
        for (File f : Objects.requireNonNull(hero.listFiles())) {
            try (BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath()))) {
                cards.add(gson.fromJson(br, Card.class));
            }
        }
    }

    public static Card getCardByName(String cardName) {
        for (ArrayList<Card> value : cards.values()) {
            for (Card card : value) {
                if(card.getName().equals(cardName)){
                    return card;
                }
            }
        }
        return null;
    }
}
