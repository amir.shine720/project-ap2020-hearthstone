package logger;

import data.Player;
import enums.LogEvent;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MyLogger {
    static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    private MyLogger() {
    }

    public static void addPlayerLog() {
        File file = getPlayerFile();
        try (FileWriter writer = new FileWriter(file)) {
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.println(String.format("USER: %s", Player.getInstance().getUsername()));
            printWriter.println(String.format("CREATED_AT: %s", dateTimeFormatter.format(LocalDateTime.now())));
            printWriter.println(String.format("PASSWORD: %s", Player.getInstance().getEncryptedPassword()));
            printWriter.println(" ");
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
//            MyLogger.log(LogEvent.ERROR, e.getMessage());
        }
    }

    private static File getPlayerFile() {
        return new File(String.format("src/main/resources/logs/%s-%s.log", Player.getInstance().getUsername(),
                Player.getInstance().getID()));
    }

    public static void deletePlayer() {
        File file = getPlayerFile();
        try /*(BufferedReader bufferedReader = new BufferedReader(new FileReader(file)))*/ {
            StringBuilder content = new StringBuilder();
//            while ((line = bufferedReader.readLine()) != null) {
//                content.append(line);
//                content.append("\n");
//            }
            for(String line : Files.readAllLines(file.toPath())){
                content.append(line).append("\n");
            }
            String[] lines = content.toString().split("\n");
            lines[3] = String.format("DELETED_AT: %s", dateTimeFormatter.format(LocalDateTime.now()));
            try (FileWriter fileWriter = new FileWriter(file)) {
                fileWriter.write(String.join("\n", lines));
            }
        } catch (IOException e) {
            e.printStackTrace();
//            MyLogger.log(LogEvent.ERROR, e.getMessage());
        }

    }


    public static void log(LogEvent event, String message) {
        try {
            String content;
            content = String.format("%s: %s %s%n", event.toString(), dateTimeFormatter.format(LocalDateTime.now()),
                    message);
            Files.write(Paths.get(getPlayerFile().toURI()), content.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
            addPlayerLog();
//            log(event, message);
        }
    }
}

