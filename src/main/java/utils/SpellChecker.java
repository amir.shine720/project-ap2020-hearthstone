package utils;

import java.util.List;

public class SpellChecker {
    public static String suggest(List<String> possibleValues, String currentValue){
        for(String possibleWord : possibleValues){
            int lastIndex = 0;
            int matches = 0;
            for(char c : currentValue.toLowerCase().toCharArray()){
                if(possibleWord.toLowerCase().substring(lastIndex).indexOf(c)!=-1){
                    lastIndex += possibleWord.substring(lastIndex).indexOf(c) + 1;
                    matches += 1;
                }
                if(matches > (int)(0.7*possibleWord.length())){
                    return possibleWord;
                }
            }
        }
        return " ";
    }
}
