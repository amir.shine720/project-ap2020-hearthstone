package utils;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class ImageGetter {




    public static ImageIcon createImageIcon(String path,String description){
        return createImageIcon(path, description, 15, 15);
    }

    public static ImageIcon createImageIcon(String path,
                                        String description, int width, int height) {
        File imgFile = new File(path);
        if (imgFile.exists()) {
            ImageIcon coinIcon = new ImageIcon(path, description);
            Image image = coinIcon.getImage();
            Image newImage = image.getScaledInstance(width,height,Image.SCALE_SMOOTH);
            coinIcon = new ImageIcon(newImage);
            return coinIcon;
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    public static ImageIcon getImage(String path, String description){
        return createImageIcon(path, description, 150,150);
    }
}
