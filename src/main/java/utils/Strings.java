package utils;

public class Strings {
    public static final String CARDS_TO_ADD = "Cards to add";
    public static final String ADD_CARD_TO_DECK = "add a card to deck";
    public static final String SAME_CARD_CAP_ERROR = "Sry, you can't have more than two of a same card in your deck!";
    public static final String ERROR = "Error";
    public static final String SELL = "Sell";
    public static final String PURCHASE = "Purchase";
}
