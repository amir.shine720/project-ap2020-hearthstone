package utils;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class Password {
    private static String seed = "Hearthstone";
    private static StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();


    public static String encrypt(String text) {
        if (!encryptor.isInitialized()) {
            encryptor.setPassword(seed);
        }
        return encryptor.encrypt(text);
    }

    public static String decrypt(String text) {
        if (!encryptor.isInitialized()) {
            encryptor.setPassword(seed);
        }
        return encryptor.decrypt(text);
    }
}
