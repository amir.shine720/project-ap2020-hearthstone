package hero;

public class HeroPower {
    private String name;
    private String description;
    private int manaCost;

    public HeroPower(String name, String description, int manaCost) {
        this.name = name;
        this.description = description;
        this.manaCost = manaCost;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getManaCost() {
        return manaCost;
    }
}
