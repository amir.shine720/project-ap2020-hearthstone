package hero;

public class SpecialPower {
    private String description;

    public SpecialPower(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
