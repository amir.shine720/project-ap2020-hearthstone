package cards;

import enums.Hero;
import enums.Rarity;
import enums.Type;

import javax.management.relation.RelationNotFoundException;
import java.sql.PreparedStatement;
import java.util.Random;

public class Card implements Comparable {
    String name;

    String description;
    int attack;
    int health;
    int manaCost;
    int playCount;

    Type type;

    Hero hero;

    Rarity rarity;

    public int getAttack() {
        return attack;
    }

    public int getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    public Hero getHero() {
        return hero;
    }

    public int getValue() {
        return rarity.getValue();
    }

    public int getManaCost() {
        return manaCost;
    }

    public String getDescription() {
        return description;
    }

    public int getPlayCount() {
        return playCount;
    }

    public void setPlayCount(int playCount) {
        this.playCount = playCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        return this.name.equals(((Card) obj).name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public Rarity getRarity() {
        return rarity;
    }

    public Type getType() {
        return type;
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) {
            return 0;
        }
        Card otherCard = (Card) o;
        int result = Integer.compare(this.getPlayCount(), otherCard.getPlayCount());
        if (result != 0) {
            return result;
        }
        result = Integer.compare(this.getRarity().getValue(), otherCard.getRarity().getValue());
        if (result != 0) {
            return result;
        }
        result = Integer.compare(this.getManaCost(), otherCard.getManaCost());
        if (result != 0) {
            return result;
        }
        if (this.getType() == Type.MINION) {
            return 1;
        } else if (otherCard.getType() == Type.MINION) {
            return -1;
        }
        Random random = new Random(System.nanoTime());
        return random.nextBoolean() ? 1 : -1;
    }
}
