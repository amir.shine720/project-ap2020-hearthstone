package cards;

import enums.Hero;

import java.util.HashMap;
import java.util.List;

public class Deck {
    String name;
    Hero hero;
    int winCount;
    List<Card> cards;
    private int gamesCount;

    // Due to high json file size, we keep card name here instead of card object itself
    private HashMap<String, Integer> cardPlayCount;

    public Deck(String name, Hero hero, List<Card> cards) {
        this.name = name;
        this.cards = cards;
        this.hero = hero;
        gamesCount = 0;
        initializeCardPlayCount();
    }

    private void initializeCardPlayCount() {
        cardPlayCount = new HashMap<>();
        for (Card card : cards) {
            cardPlayCount.put(card.getName(), 0);
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public int getWinCount() {
        return winCount;
    }

    public void setWinCount(int winCount) {
        this.winCount = winCount;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void addCardToDeck(Card card) {
        cards.add(card);
    }

    public void removeCardFromDeck(Card card) {
        cards.remove(card);
    }

    public int getGamesCount() {
        return gamesCount;
    }

    public void addPlayCard(Card card){
        cardPlayCount.put(card.getName(), cardPlayCount.get(card.getName()) + 1);
    }

    public Integer getCardPlayCount(Card card){
        return cardPlayCount.get(card.getName());
    }
}
