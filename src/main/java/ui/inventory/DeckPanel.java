package ui.inventory;

import cards.Deck;
import ui.inventory.listeners.DeckClickListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DeckPanel extends JPanel {
    DeckClickListener deckClickListener;
    Deck deck;

    public DeckPanel(Deck deck) {
        super();

        this.deck = deck;

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        setBorder(BorderFactory.createEtchedBorder());

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.CENTER;
        gc.weighty = 1;
        gc.weightx = 1;

        gc.gridy = 0;
        add(new JLabel(deck.getName()),gc);

        gc.gridy++;
        add(new JLabel(String.valueOf(deck.getHero())),gc);

    }

    private void setMouseListener() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                deckClickListener.deckChangeRequired(deck);
            }
        });
    }

    public void setDeckClickListener(DeckClickListener deckClickListener) {
        this.deckClickListener = deckClickListener;
        setMouseListener();

    }
}
