package ui.inventory;

import cards.Card;
import cards.Deck;
import data.InventoryDataModel;
import data.Player;
import enums.Hero;
import enums.LogEvent;
import logger.MyLogger;
import ui.terminal.TerminalViewModel;
import utils.FullDeckException;
import utils.SameCardLimitException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InventoryViewModel {
    InventoryDataModel dataModel = new InventoryDataModel();

    public void changePlayerHero(Hero newHero) {
        Player.getInstance().setHero(newHero);

    }

    public ArrayList<Card> getPlayerCards() {
        return Player.getInstance().getCards();
    }

    public Deck getPlayerDeck() {
        return Player.getInstance().getCurrentDeck();
    }

    public ArrayList<Card> getNotOwnedCards() {
        ArrayList<Card> notOwnedCards = new ArrayList<>();
        for (Card card : getAllCards()) {
            if (!getPlayerCards().contains(card)) {
                notOwnedCards.add(card);
            }
        }
        return notOwnedCards;
    }

    public void addCardToDeck(Card card) throws FullDeckException, SameCardLimitException {

        if(Collections.frequency(getPlayerDeck().getCards(), card) >= 2){
            throw new SameCardLimitException();
        }

        Player.getInstance().addCardToDeck(card);
        MyLogger.log(LogEvent.ADD, String.format("card:%s", card.getName()));
//        }
    }

    public void removeCardFromDeck(Card card) {
        Player.getInstance().removeCardFromCurrentDeck(card);
        MyLogger.log(LogEvent.REMOVE, String.format("card:%s", card.getName()));
    }

    public ArrayList<Card> getAllCards() {
        TerminalViewModel viewModel = new TerminalViewModel();
        return (ArrayList<Card>) viewModel.getAllCards();
    }

    public List<Card> getCardsWithFilter() {
        ArrayList<Card> cards = getAllCards();
        if (!CardFilter.getCardFilter().isNotOwned()) {
            cards.removeAll(getNotOwnedCards());
        }
        if (!CardFilter.getCardFilter().isOwned()) {
            cards.removeAll(getPlayerCards());
        }
        ArrayList<Card> filteredCards = new ArrayList<>();
        for (Card card : cards) {
            if (card.getName().toLowerCase().contains(CardFilter.getCardFilter().getName().toLowerCase()) &&
                    card.getHero() == (CardFilter.getCardFilter().getHero()) &&
                    (CardFilter.getCardFilter().getMana() == -1 || card.getManaCost() == CardFilter.getCardFilter().getMana())) {
                filteredCards.add(card);
            }
        }
        return filteredCards;
    }

    public ArrayList<Deck> getAllPlayerDecks() {
        return Player.getInstance().getDecks();
    }

    public void setPlayerCurrentDeck(Deck deck) {
        Player.getInstance().setCurrentDeck(deck);
    }

    public ArrayList<Card> getAddableCardsToDeck() {
        return getAddableCardsToDeck(getPlayerDeck());
    }

    public ArrayList<Card> getAddableCardsToDeck(Deck deck) {
        ArrayList<Card> availableCards = new ArrayList<>();
        for (Card card : Player.getInstance().getCards()) {
            if ((card.getHero() == deck.getHero() || card.getHero() == Hero.NATURAL) /*&& !deck.getCards().contains(card)*/) {
                availableCards.add(card);
            }
        }
        return availableCards;
    }

    public ArrayList<Hero> getAllHeroes() {
        return Hero.getAllHeroes();
    }

    public void createEmptyDeck(Hero hero, String name) {
        Deck deck = new Deck(name, hero, new ArrayList<>());
        Player.getInstance().addDeck(deck);
        Player.getInstance().setCurrentDeck(deck);
        Player.getInstance().setHero(hero);
        MyLogger.log(LogEvent.CREATE_DECK, "deck created");
    }

    public void deleteDeck(Deck deck) {
        Player.getInstance().getDecks().remove(deck);
    }

    public void renameCurrentDeck(String newName) {
        Player.getInstance().getCurrentDeck().setName(newName);
    }

    public void setPlayerHero(Hero hero) {
        Player.getInstance().setHero(hero);
    }
}
