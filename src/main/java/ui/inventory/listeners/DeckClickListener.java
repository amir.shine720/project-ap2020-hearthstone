package ui.inventory.listeners;

import cards.Deck;

public interface DeckClickListener {
    void deckChangeRequired(Deck deck);
}
