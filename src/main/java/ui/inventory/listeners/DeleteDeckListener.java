package ui.inventory.listeners;

import cards.Deck;

public interface DeleteDeckListener {
    void deckDeleted(Deck deck);
}
