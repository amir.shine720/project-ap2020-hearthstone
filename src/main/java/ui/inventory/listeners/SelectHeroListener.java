package ui.inventory.listeners;

import enums.Hero;

public interface SelectHeroListener  {

    void heroSelected(Hero hero, String name);
}
