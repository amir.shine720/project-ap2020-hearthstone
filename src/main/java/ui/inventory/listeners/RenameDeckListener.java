package ui.inventory.listeners;

public interface RenameDeckListener {
    void deckRenamed(String newName);
}
