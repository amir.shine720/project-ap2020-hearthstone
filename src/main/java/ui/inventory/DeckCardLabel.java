package ui.inventory;

import cards.Card;
import ui.card.CardLabel;
import ui.store.listeners.DeckTransmissionListener;
import ui.store.StoreViewModel;

import javax.swing.*;
import java.util.ArrayList;

public class DeckCardLabel extends CardLabel {

    public DeckCardLabel(Card card) {
        super(card);
    }

    @Override
    protected void initRequiredComponents(Card card) {
        StoreViewModel viewModel = new StoreViewModel();
        cardNameLabel = new JLabel(card.getName());
        cardHeroLabel = new JLabel(String.valueOf(card.getHero()));
        cardManaLabel = new JLabel(String.valueOf(card.getManaCost()),
                viewModel.getManaIcon(), SwingConstants.CENTER);
        cardValueLabel = new JLabel(String.valueOf(card.getValue()),
                viewModel.getCoinsIcon(), SwingConstants.CENTER);
        cardHealthLabel = new JLabel(String.valueOf(card.getHealth()),
                viewModel.getHealthIcon(), SwingConstants.CENTER);
        cardAttackLabel = new JLabel(String.valueOf(card.getAttack()),
                viewModel.getAttackIcon(), SwingConstants.CENTER);
        cardDescriptionLabel = new JTextArea(card.getDescription());
        cardCountLabel = new JLabel(String.valueOf(cardCount));
        removeButton = new JButton("Remove");
    }

    @Override
    protected void fillSatComponentsIntoArray(ArrayList<JComponent> components) {
        components.add(cardNameLabel);
        components.add(cardHeroLabel);
        components.add(cardManaLabel);
        components.add(cardValueLabel);
        components.add(cardHealthLabel);
        components.add(cardAttackLabel);
        components.add(cardDescriptionLabel);
        components.add(removeButton);
        components.add(cardCountLabel);
    }

    public void setCardClickListener(DeckTransmissionListener deckTransmissionListener) {
        removePreviousActionListeners(removeButton);
        removeButton.addActionListener(actionEvent -> deckTransmissionListener.removeCardFromDeck(card));
    }

    public void hideButtons() {
        removeButton.setVisible(false);
    }

    public void showButtons() {
        removeButton.setVisible(true);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if (obj.getClass() == this.getClass()) {
            return cardNameLabel.getText().equals(((DeckCardLabel) obj).cardNameLabel.getText());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return cardNameLabel.getText().hashCode();
    }
}
