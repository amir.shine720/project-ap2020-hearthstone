package ui.inventory;

import enums.Hero;

public class CardFilter {
    private boolean owned;
    private boolean notOwned;
    private Hero hero;
    private int mana;
    private String name;
    private static CardFilter cardFilter;

    public static CardFilter getCardFilter() {
        if (cardFilter == null) {
            cardFilter = new CardFilter();
        }
        return cardFilter;
    }

    public static void setCardFilter(boolean owned, boolean notOwned, Hero hero, int mana, String name) {
        cardFilter.setOwned(owned);
        cardFilter.setNotOwned(notOwned);
        cardFilter.setHero(hero);
        cardFilter.setMana(mana);
        cardFilter.setName(name);
    }

    private CardFilter() {
        owned = true;
        notOwned = true;
        hero = Hero.NATURAL;
        mana = -1;
        name = "";
    }

    public static void reset() {
        cardFilter = new CardFilter();
    }

    public boolean isOwned() {
        return owned;
    }

    public void setOwned(boolean owned) {
        this.owned = owned;
    }

    public boolean isNotOwned() {
        return notOwned;
    }

    public void setNotOwned(boolean notOwned) {
        this.notOwned = notOwned;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
