package ui.inventory;

import cards.Card;
import ui.card.CardLabel;
import ui.store.listeners.DeckTransmissionListener;
import ui.store.StoreViewModel;

import javax.swing.*;
import java.util.ArrayList;

public class InventoryCardLabel extends CardLabel {

    public InventoryCardLabel(Card card) {
        super(card);
    }

    @Override
    protected void initRequiredComponents(Card card) {
        StoreViewModel viewModel = new StoreViewModel();
        cardNameLabel = new JLabel(card.getName());
        cardHeroLabel = new JLabel(String.valueOf(card.getHero()));
        cardManaLabel = new JLabel(String.valueOf(card.getManaCost()),
                viewModel.getManaIcon(), SwingConstants.CENTER);
        cardValueLabel = new JLabel(String.valueOf(card.getValue()),
                viewModel.getCoinsIcon(), SwingConstants.CENTER);
        cardHealthLabel = new JLabel(String.valueOf(card.getHealth()),
                viewModel.getHealthIcon(), SwingConstants.CENTER);
        cardAttackLabel = new JLabel(String.valueOf(card.getAttack()),
                viewModel.getAttackIcon(), SwingConstants.CENTER);
        cardDescriptionLabel = new JTextArea(card.getDescription());
        addButton = new JButton("Add");
    }

    @Override
    protected void fillSatComponentsIntoArray(ArrayList<JComponent> components) {
        components.add(cardNameLabel);
        components.add(cardHeroLabel);
        components.add(cardManaLabel);
        components.add(cardValueLabel);
        components.add(cardHealthLabel);
        components.add(cardAttackLabel);
        components.add(cardDescriptionLabel);
        components.add(addButton);
//        components.add(removeButton);
    }

    public void setCardClickListener(DeckTransmissionListener deckTransmissionListener) {
        removePreviousActionListeners(addButton);
        addButton.addActionListener(actionEvent -> deckTransmissionListener.addCardToDeck(card));
    }

    public void hideButtons() {
        addButton.setVisible(false);
    }

    public void showButtons() {
        addButton.setVisible(true);
    }

}
