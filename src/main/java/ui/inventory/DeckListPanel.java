package ui.inventory;

import cards.Deck;
import data.Player;
import enums.Hero;
import ui.inventory.listeners.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class DeckListPanel extends JPanel {
    private ArrayList<DeckPanel> deckPanels;
    private JButton addDeckButton;
    private JButton renameDeckButton;
    private JButton deleteDeckButton;
    private JButton doneButton;
    private SelectHeroDialog selectHeroDialog;
    private DeckRenameDialog deckRenameDialog;
    static int selectedIndex = -1;

    InventoryViewModel viewModel;

    public DeckListPanel() {
        super();

        viewModel = new InventoryViewModel();

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBorder(BorderFactory.createTitledBorder("Decks"));

        deckPanels = new ArrayList<>();
        addDeckButton = new JButton("New Deck");
        selectHeroDialog = new SelectHeroDialog();
        deckRenameDialog = new DeckRenameDialog();
        renameDeckButton = new JButton("Rename Deck");
        deleteDeckButton = new JButton("Delete Deck");
        doneButton = new JButton("Done");

        // Means show button when user selects a deck
        renameDeckButton.setVisible(selectedIndex != -1);
        deleteDeckButton.setVisible(selectedIndex != -1);
        doneButton.setVisible(selectedIndex != -1);

        for (Deck deck : viewModel.getAllPlayerDecks()) {
            DeckPanel deckPanel = new DeckPanel(deck);
            deckPanels.add(deckPanel);
        }
        setListeners();
    }

    private void setListeners() {
        addDeckButton.addActionListener(actionEvent -> selectHeroDialog.setVisible(true));
        renameDeckButton.addActionListener(actionEvent -> deckRenameDialog.showDialog(getSelectedDeck()));
    }

    public void setDeckClickListener(DeckClickListener deckClickListener) {
        for (DeckPanel deckPanel : deckPanels) {
            deckPanel.setDeckClickListener(deckClickListener);
            if (deckPanels.indexOf(deckPanel) == selectedIndex) {
                deckPanel.setBackground(Color.CYAN);
            }
            add(deckPanel);
        }
        add(addDeckButton);
        add(renameDeckButton);
        add(deleteDeckButton);
        add(doneButton);
    }

    public static Deck getSelectedDeck() {
        return Player.getInstance().getDecks().get(selectedIndex);
    }

    public void setDoneVisibility(boolean status) {
        doneButton.setVisible(status);
    }

    public void setDoneClickListener(DeckListDoneListener deckListDoneListener) {
        doneButton.addActionListener(actionEvent -> deckListDoneListener.donePressed());
    }

    public void setSelectHeroListener(SelectHeroListener selectHeroListener) {
        selectHeroDialog.setSelectHeroListener(selectHeroListener);
    }


    public void setDeleteDeckListener(DeleteDeckListener deleteDeckListener){
        deleteDeckButton.addActionListener(actionEvent -> deleteDeckListener.deckDeleted(getSelectedDeck()));
    }

    public void setDeckRenameDialogListener(RenameDeckListener renameDeckListener){
        deckRenameDialog.setRenameDeckListener(renameDeckListener);
    }

    private void setRenameDeckButtonListener(){
//        renameDeckButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent actionEvent) {
//                renameDeckListener.deckRenamed();
//            }
//        });
    }

    private static class SelectHeroDialog extends JDialog implements ActionListener {
        JButton okButton;
        JComboBox<String> heroListComboBox;

        private SelectHeroListener selectHeroListener;
        private JTextField nameTextField;

        public SelectHeroDialog() {

            setLayout(new GridBagLayout());

            heroListComboBox = getHeroComboBox();
            okButton = new JButton("OK");
            nameTextField = new JTextField(10);

            okButton.addActionListener(this);

            addComponents();
            setVisible(false);
            pack();
            setLocationRelativeTo(null);
        }

        private void addComponents() {
            GridBagConstraints gc = new GridBagConstraints();
            gc.gridx = 0;
            gc.gridy = 0;
            gc.weighty = 1;
            gc.weightx = 4;
            gc.anchor = GridBagConstraints.LAST_LINE_END;
            add(new JLabel("Select a name for your deck: "), gc);

            gc.gridy++;
            add(nameTextField, gc);

            gc.gridy++;
            add(new JLabel("Select a hero for your deck: "), gc);

            gc.gridy++;
            add(heroListComboBox, gc);

            gc.gridx = 1;
            gc.anchor = GridBagConstraints.LAST_LINE_START;
            gc.weightx = 1;
            add(okButton, gc);
        }


        private JComboBox<String> getHeroComboBox() {
            JComboBox<String> heroListComboBox;
            heroListComboBox = new JComboBox<>();
            DefaultComboBoxModel<String> comboBoxModel = new DefaultComboBoxModel<>();
            for (Hero hero : Hero.getAllHeroes()) {
                if (hero == Hero.NATURAL) {
                    continue;
                }
                comboBoxModel.addElement(hero.getName());
            }
            heroListComboBox.setModel(comboBoxModel);
            return heroListComboBox;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            selectHeroListener.heroSelected(Hero.getByName(((String) heroListComboBox.getSelectedItem())),
                    nameTextField.getText());
            setVisible(false);
        }

        public void setSelectHeroListener(SelectHeroListener selectHeroListener) {
            this.selectHeroListener = selectHeroListener;
        }

    }

    private static class DeckRenameDialog extends JDialog implements ActionListener {
        JTextField renameTextField;
        JButton doneButton;
        RenameDeckListener renameDeckListener;

        public void showDialog(Deck currentDeck){
            renameTextField = new JTextField(currentDeck.getName());
            doneButton = new JButton("Done");
            doneButton.addActionListener(this);

            setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
            add(new JLabel("Enter your new name: "));
            add(renameTextField);
            add(doneButton);
            pack();
            setLocationRelativeTo(null);
            setVisible(true);
        }

        public void setRenameDeckListener(RenameDeckListener renameDeckListener) {
            this.renameDeckListener = renameDeckListener;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            renameDeckListener.deckRenamed(renameTextField.getText());
            setVisible(false);
        }
    }
}
