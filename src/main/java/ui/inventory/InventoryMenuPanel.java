package ui.inventory;

import enums.Hero;
import ui.inventory.listeners.FilterEventListener;
import ui.login.listeners.BackListener;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public class InventoryMenuPanel extends JPanel {
    JTextField searchTextField;
    JComboBox<Hero> heroComboBox;
    JCheckBox ownedCheckBox;
    JCheckBox notOwnedCheckBox;
    JComboBox<String> manaComboBox;
    JButton filterButton;
    JButton backButton;

    InventoryViewModel viewModel;

    private FilterEventListener filterEventListener;
    private BackListener backListener;

    public InventoryMenuPanel() {
        super();

        viewModel = new InventoryViewModel();

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder("Filters"));

        searchTextField = new JTextField(10);
        heroComboBox = getHeroJComboBox();
        ownedCheckBox = new JCheckBox("Owned cards", CardFilter.getCardFilter().isOwned());
        notOwnedCheckBox = new JCheckBox("Not owned cards", CardFilter.getCardFilter().isNotOwned());
        manaComboBox = getManaComboBox();
        filterButton = new JButton("Filter");
        backButton = new JButton("Back");


        backButton.addActionListener(actionEvent -> backListener.backPressed());

        filterButton.addActionListener(actionEvent -> {
            String manaCostString = (String) manaComboBox.getSelectedItem();

            CardFilter.setCardFilter(ownedCheckBox.isSelected(), notOwnedCheckBox.isSelected(),
                    (Hero) heroComboBox.getSelectedItem(),
                    Objects.equals(manaCostString, "Any") ? -1 : Integer.parseInt(manaCostString),
                    searchTextField.getText());
            filterEventListener.filtered();
        });


        GridBagConstraints gc = new GridBagConstraints();
        gc.weightx = 1;
        gc.weighty = 1;

        gc.gridy = 0;
        ////////////// First Row /////////////
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Name: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(searchTextField, gc);

        ////////////// Next Row /////////////
        gc.gridy++;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Class: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(heroComboBox, gc);

        ////////////// Next Row /////////////
        gc.gridy++;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Mana cost: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(manaComboBox, gc);

        ////////////// Next Row /////////////
        gc.gridy++;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.LAST_LINE_END;
        gc.insets = new Insets(0, 0, 5, 0);
        add(new JLabel("Cards:"), gc);

        gc.weighty = 0.1;

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        gc.insets = new Insets(0, 0, 0, 0);

        add(ownedCheckBox, gc);


        gc.gridy++;
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        add(notOwnedCheckBox, gc);

        ////////////// Next Row /////////////
        gc.weighty = 1;
        gc.gridy++;


        gc.gridx = 1;
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        gc.insets = new Insets(0, 0, 5, 0);
        add(filterButton, gc);

        ////////////// Final BackButton Row /////////////
        gc.gridy++;
        gc.weighty = 10;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        add(backButton, gc);

    }

    public void setFilterEventListener(FilterEventListener filterEventListener) {
        this.filterEventListener = filterEventListener;
    }

    private JComboBox<String> getManaComboBox() {
        JComboBox<String> manaComboBox;
        manaComboBox = new JComboBox<>();

        DefaultComboBoxModel<String> manaModel = new DefaultComboBoxModel<>();

        manaModel.addElement("Any");
        for (int i = 1; i < 11; i++) {
            manaModel.addElement(String.valueOf(i));
        }
        manaComboBox.setModel(manaModel);
        manaComboBox.setSelectedItem(CardFilter.getCardFilter().getMana());
        return manaComboBox;
    }

    private JComboBox<Hero> getHeroJComboBox() {
        JComboBox<Hero> heroComboBox;
        heroComboBox = new JComboBox<>();

        DefaultComboBoxModel<Hero> heroModel = new DefaultComboBoxModel<>();
        for (Hero hero : viewModel.getAllHeroes()) {
            heroModel.addElement(hero);
        }
//        heroModel.addElement(Hero.MAGE);
//        heroModel.addElement(Hero.ROGUE);
//        heroModel.addElement(Hero.WARLOCK);
        heroComboBox.setModel(heroModel);
        heroComboBox.setSelectedItem(CardFilter.getCardFilter().getHero());
        return heroComboBox;
    }

    public void setBackListener(BackListener backListener) {
        this.backListener = backListener;
    }

    public void setEnterToSubmit() {
        getRootPane().setDefaultButton(filterButton);
    }

}
