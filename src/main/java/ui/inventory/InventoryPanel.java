package ui.inventory;

import cards.Card;
import enums.LogEvent;
import enums.Section;
import logger.MyLogger;
import ui.BasicPanel;
import ui.CardsPanel;
import ui.card.CardType;
import ui.store.StoreViewModel;
import ui.store.listeners.DeckTransmissionListener;
import utils.FullDeckException;
import utils.SameCardLimitException;
import utils.Strings;

import javax.swing.*;
import java.awt.*;

public class InventoryPanel extends BasicPanel {
    CardsPanel cardPanel;
    InventoryMenuPanel menuPanel;
    DeckListPanel deckListPanel;
    CardsPanel deckCardsPanel;
    InventoryViewModel viewModel;

    public InventoryPanel() {
        super("Collections");

        viewModel = new InventoryViewModel();

        setLayout(new BorderLayout());
        menuPanel = new InventoryMenuPanel();
        cardPanel = new CardsPanel(viewModel.getCardsWithFilter(), 3, "Card", CardType.INVENTORY, true);
        deckListPanel = new DeckListPanel();
        deckCardsPanel = new CardsPanel(viewModel.getPlayerDeck().getCards(), 15, CardType.DECK);

        deckCardsPanel.setVisible(false);
        cardPanel.hideButtons();

        setListeners();

        addComponents();
    }

    private void addComponents() {
        add(menuPanel, BorderLayout.WEST);
        add(getHorizontalScrollPane(cardPanel), BorderLayout.CENTER);
        add(deckListPanel, BorderLayout.EAST);
        add(getVerticalScrollPane(deckCardsPanel), BorderLayout.SOUTH);
    }

    private void setListeners() {
        menuPanel.setFilterEventListener(() -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "filter button");
            cardPanel = new CardsPanel(viewModel.getCardsWithFilter(), CardType.INVENTORY);
            cardPanel.hideButtons();
            DeckListPanel.selectedIndex = -1;
            refresh();
        });
        menuPanel.setBackListener(() -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "back button");
            nextSectionListener.nextSectionRequired(Section.TERMINAL);
        });

        // Don't remove this, otherwise lists stack on each other
        deckListPanel = new DeckListPanel();

        deckListPanel.setDeckClickListener(deck -> {
            MyLogger.log(LogEvent.CLICK_DECK, "select deck");
            DeckListPanel.selectedIndex = viewModel.getAllPlayerDecks().indexOf(deck);
            cardPanel = new CardsPanel(viewModel.getAddableCardsToDeck(deck), 3, Strings.CARDS_TO_ADD, CardType.INVENTORY);
            deckCardsPanel = new CardsPanel(deck.getCards(), 15, "Deck", CardType.DECK);
            deckCardsPanel.setVisible(true);
            cardPanel.showButtons();
            menuPanel.setVisible(false);
            deckListPanel.setDoneVisibility(true);
            viewModel.setPlayerCurrentDeck(deck);
            viewModel.setPlayerHero(deck.getHero());
            refresh();
        });

        deckListPanel.setDoneClickListener(() -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "done button");
            deckCardsPanel.setVisible(false);
            menuPanel.setVisible(true);
            deckListPanel.setDoneVisibility(false);
            cardPanel = new CardsPanel(viewModel.getCardsWithFilter(), CardType.INVENTORY);
            cardPanel.hideButtons();
            DeckListPanel.selectedIndex = -1;
            refresh();
        });

        deckListPanel.setDeckRenameDialogListener(newName -> {
            viewModel.renameCurrentDeck(newName);
            deckListPanel = new DeckListPanel();
            refresh();
        });

        deckListPanel.setSelectHeroListener((hero, name) -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "new deck button");
            menuPanel.setVisible(false);
            deckCardsPanel.setVisible(true);
            viewModel.createEmptyDeck(hero, name);
            cardPanel = new CardsPanel(viewModel.getAddableCardsToDeck(), CardType.INVENTORY);
            deckCardsPanel = new CardsPanel(viewModel.getPlayerDeck().getCards(), 15, CardType.DECK);
            DeckListPanel.selectedIndex = viewModel.getAllPlayerDecks().indexOf(viewModel.getPlayerDeck());
            refresh();
        });

        deckListPanel.setDeleteDeckListener(deck -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "delete deck button");
            int result = JOptionPane.showConfirmDialog(InventoryPanel.this,
                    "Are you sure you want to delete the selected deck?", "Delete Deck", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                viewModel.deleteDeck(deck);
                viewModel.setPlayerCurrentDeck(viewModel.getAllPlayerDecks().get(DeckListPanel.selectedIndex));
            }
            deckListPanel = new DeckListPanel();
            cardPanel = new CardsPanel(viewModel.getAddableCardsToDeck(DeckListPanel.getSelectedDeck()),
                    3, Strings.CARDS_TO_ADD, CardType.INVENTORY);
            deckCardsPanel = new CardsPanel(DeckListPanel.getSelectedDeck().getCards()
                    , 15, "Deck", CardType.DECK);
            refresh();
        });

        cardPanel.setCardClickListener(card -> {
            StoreViewModel storeViewModel = new StoreViewModel();
            if (storeViewModel.getAvailableCards().contains(card)) {
                nextSectionListener.nextSectionRequired(Section.STORE);
            }
        });

        cardPanel.setCardClickListener(new DeckTransmissionListener() {
            @Override
            public void addCardToDeck(Card card) {
                try {
                    MyLogger.log(LogEvent.CLICK_CARD, Strings.ADD_CARD_TO_DECK);
                    viewModel.addCardToDeck(card);
                    deckCardsPanel = new CardsPanel(DeckListPanel.getSelectedDeck().getCards()
                            , 15, "Deck", CardType.DECK);
                    cardPanel = new CardsPanel(viewModel.getAddableCardsToDeck(DeckListPanel.getSelectedDeck()),
                            3, Strings.CARDS_TO_ADD, CardType.INVENTORY);
                    refresh();
                } catch (FullDeckException e) {
                    MyLogger.log(LogEvent.ERROR, "full deck");
                    JOptionPane.showMessageDialog(InventoryPanel.this, "Sry you deck is full!"
                            , "Full deck", JOptionPane.ERROR_MESSAGE);
                } catch (SameCardLimitException e) {
                    MyLogger.log(LogEvent.ERROR, "same card cap limit reached");
                    JOptionPane.showMessageDialog(InventoryPanel.this,
                            Strings.SAME_CARD_CAP_ERROR
                            , "Same Card Cap Reached", JOptionPane.ERROR_MESSAGE);
                }
            }

            // TODO: some refactoring is required
            @Override
            public void removeCardFromDeck(Card card) {

            }
        });

        deckCardsPanel.setCardClickListener(new DeckTransmissionListener() {
            // TODO: some refactoring is required
            @Override
            public void addCardToDeck(Card card) {

            }

            @Override
            public void removeCardFromDeck(Card card) {
                MyLogger.log(LogEvent.CLICK_CARD, "remove card from deck");
                viewModel.removeCardFromDeck(card);
                deckCardsPanel = new CardsPanel(DeckListPanel.getSelectedDeck().getCards()
                        , 15, "Deck", CardType.DECK);
                cardPanel = new CardsPanel(viewModel.getAddableCardsToDeck(DeckListPanel.getSelectedDeck()),
                        3, Strings.CARDS_TO_ADD, CardType.INVENTORY);
                refresh();
            }
        });
    }

    private JScrollPane getHorizontalScrollPane(JPanel panel) {
        JScrollPane cardPanelScrollPane = new JScrollPane(panel);
        cardPanelScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        return cardPanelScrollPane;
    }

    private JScrollPane getVerticalScrollPane(JPanel panel) {
        JScrollPane cardPanelScrollPane = new JScrollPane(panel);
        cardPanelScrollPane.setVisible(panel.isVisible());
        cardPanelScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        Dimension d = cardPanelScrollPane.getPreferredSize();
        d.height += 20;
        cardPanelScrollPane.setPreferredSize(d);
        return cardPanelScrollPane;
    }

    private void refresh() {
        removeAll();
        revalidate();
        repaint();
        setListeners();
        addComponents();
    }
}
