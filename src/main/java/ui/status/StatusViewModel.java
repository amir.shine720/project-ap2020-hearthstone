package ui.status;

import cards.Card;
import cards.Deck;
import data.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StatusViewModel {
    public String getAverageManaCost(Deck deck) {
        int manaSum = 0;
        for (Card card : deck.getCards()) {
            manaSum += card.getManaCost();
        }
        return String.valueOf(manaSum / deck.getCards().size());
    }

    public String getWinRate(Deck deck) {
        if (deck.getGamesCount() == 0) {
            return "0";
        }
        return String.valueOf(deck.getWinCount() / deck.getGamesCount());
    }

    public Deck getDeckFromName(String deckName) {
        for (Deck deck : Player.getInstance().getDecks()) {
            if (deck.getName().equals(deckName)) {
                return deck;
            }
        }
        return null;
    }

    public Card getMostPlayedCard(Deck deck) {
        List<Card> cards = deck.getCards();
        Card maxCard = cards.get(0);
        for (Card card : cards) {
            if (maxCard.compareTo(card) < 0){
                maxCard = card;
            }
        }
        return maxCard;
    }
}
