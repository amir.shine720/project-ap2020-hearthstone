package ui.status;

import cards.Deck;

import javax.swing.*;
import java.awt.*;

public class StatusDeckPanel extends JPanel {

    public StatusDeckPanel(Deck deck) {
        super();

        StatusViewModel viewModel = new StatusViewModel();

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.insets = new Insets(5,5,5,5);

        gc.weighty = 1;
        gc.weightx = 1;
        gc.gridy = 0;

        //// First Row ////
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        add(new JLabel("Name: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.WEST;
        add(new JLabel(deck.getName()), gc);

        //// Next Row ////
        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        add(new JLabel("Win Ratio: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.WEST;
        add(new JLabel(viewModel.getWinRate(deck)), gc);

        //// Next Row ////
        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        add(new JLabel("Wins: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.WEST;
        add(new JLabel(String.valueOf(deck.getWinCount())), gc);

        //// Next Row ////
        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        add(new JLabel("Total Games: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.WEST;
        add(new JLabel(String.valueOf(deck.getGamesCount())), gc);

        //// Next Row ////
        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        add(new JLabel("Average Mana Cost: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.WEST;
        add(new JLabel(viewModel.getAverageManaCost(deck)), gc);

        //// Next Row ////
        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        add(new JLabel("Hero: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.WEST;
        add(new JLabel(String.valueOf(deck.getHero())), gc);

        //// Next Row ////
        gc.gridy++;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        add(new JLabel("MostPlayedCard: "), gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.WEST;
        add(new JLabel(String.valueOf(viewModel.getMostPlayedCard(deck).getName())), gc);


        //// Final Empty Row ////
        gc.gridy++;
        gc.gridx = 0;
        gc.weighty = 10;
        add(new JLabel(), gc);
    }
}
