package ui.status;

import cards.Deck;
import data.Player;
import enums.LogEvent;
import enums.Section;
import logger.MyLogger;
import ui.BasicPanel;

import javax.swing.*;
import java.awt.*;

public class StatusPanel extends BasicPanel {
    JComboBox<String> decksComboBox;
    JButton showButton;
    JPanel menuPanel;
    StatusDeckPanel statusDeckPanel;
    private StatusViewModel viewModel;
    private JButton backButton;

    public StatusPanel() {
        super("Status");

        setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        decksComboBox = getDecksComboBox();
        showButton = new JButton("Show Deck Status");
        viewModel = new StatusViewModel();
        backButton = new JButton("Back");

        statusDeckPanel = new StatusDeckPanel(Player.getInstance().getCurrentDeck());

        showButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "deck status show button");
            statusDeckPanel = new StatusDeckPanel(
                    viewModel.getDeckFromName((String) decksComboBox.getSelectedItem()));
            removeAll();
            revalidate();
            repaint();

            addComponents();
        });

        backButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "back button");
            nextSectionListener.nextSectionRequired(Section.TERMINAL);
        });
        menuPanel = getMenuPanel();

        addComponents();
    }

    private void addComponents() {
        GridBagConstraints gc = new GridBagConstraints();
        gc.insets = new Insets(10,10,10,10);
        gc.gridx = 0;
        gc.gridy = 0;
        gc.anchor = GridBagConstraints.EAST;
        add(menuPanel, gc);

        gc.gridx = 1;
        gc.anchor = GridBagConstraints.WEST;
        add(statusDeckPanel, gc);
    }

    private JPanel getMenuPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        gc.weighty = 1;
        gc.weightx = 1;

        gc.gridx = 0;
        gc.gridy = 0;
        gc.insets = new Insets(10, 10, 10, 10);
        gc.anchor = GridBagConstraints.CENTER;
        panel.add(new JLabel("Deck:"), gc);

        gc.gridx = 1;
        gc.gridy = 0;
        gc.anchor = GridBagConstraints.CENTER;
        panel.add(decksComboBox, gc);

        gc.gridx = 1;
        gc.gridy = 1;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        panel.add(showButton, gc);

        gc.gridy = 2;
        gc.weighty = 15;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        panel.add(backButton, gc);

        return panel;
    }

    private JComboBox<String> getDecksComboBox() {
        JComboBox<String> comboBox = new JComboBox<>();
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();
        for (Deck deck : Player.getInstance().getDecks()) {
            model.addElement(deck.getName());
        }
        comboBox.setModel(model);
        comboBox.setSelectedIndex(0);
        return comboBox;
    }
}

