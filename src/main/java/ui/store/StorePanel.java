package ui.store;

import enums.LogEvent;
import enums.Section;
import logger.MyLogger;
import ui.BasicPanel;
import ui.store.listeners.PageChangeListener;
import utils.SellHeroCardException;
import utils.Strings;

import javax.swing.*;
import java.awt.*;

public class StorePanel extends BasicPanel {
    MenuPanel menuPanel;
    StoreCardsRootPanel cardPanel;
    StoreViewModel viewModel;

    public StorePanel() {
        super("Store");

        viewModel = new StoreViewModel();


        setLayout(new BorderLayout());

        setPanels();

    }

    private void setPanels() {
        initializeComponents();

        setListeners();

        addComponents();
    }

    private void initializeComponents() {
        menuPanel = new MenuPanel();
        cardPanel = new StoreCardsRootPanel();
    }

    private void addComponents() {
        add(menuPanel, BorderLayout.WEST);
        add(cardPanel, BorderLayout.CENTER);
    }

    private void refresh() {
        removeAll();
        revalidate();
        repaint();

        setPanels();
    }

    private void setListeners() {
        menuPanel.setChangeListener(new PageChangeListener() {
            @Override
            public void switchToSellPanel() {

                cardPanel.switchToSellPanel();
            }

            @Override
            public void switchToBuyPanel() {
                cardPanel.switchToBuyPanel();
            }

            @Override
            public void back() {
                nextSectionListener.nextSectionRequired(Section.TERMINAL);
            }
        });

        cardPanel.setSellCardClickListener(card -> {
            try {
                MyLogger.log(LogEvent.CLICK_CARD, "click card to sell");
                int answer = JOptionPane.showConfirmDialog(StorePanel.this,
                        String.format("Are you sure you want to sell %s card for %d coins?", card.getName(), (int) (card.getValue() * 0.75)));
                if (answer == JOptionPane.YES_OPTION) {
                    viewModel.sellCard(card);
                    JOptionPane.showMessageDialog(StorePanel.this, "Sold successfully!"
                            , Strings.SELL, JOptionPane.INFORMATION_MESSAGE);
                    refresh();
                }
            } catch (SellHeroCardException e) {
                MyLogger.log(LogEvent.ERROR, "sell hero card error");
                JOptionPane.showMessageDialog(StorePanel.this, "You can't sell a hero card!"
                        , Strings.ERROR, JOptionPane.ERROR_MESSAGE);
            }
        });

        cardPanel.setBuyCardClickListener(card -> {
            MyLogger.log(LogEvent.CLICK_CARD, "click card to buy");
            int answer = JOptionPane.showConfirmDialog(StorePanel.this,
                    String.format("Are you sure you want to buy %s card for %d coins?", card.getName(), card.getValue()));
            if (answer == JOptionPane.YES_OPTION) {
                boolean haveEnoughCoins = viewModel.purchaseCard(card);
                if (haveEnoughCoins) {
                    JOptionPane.showMessageDialog(StorePanel.this, "Purchased successfully!"
                            , Strings.PURCHASE, JOptionPane.INFORMATION_MESSAGE);
                    refresh();
                } else {
                    MyLogger.log(LogEvent.ERROR, "not enough coins");
                    JOptionPane.showMessageDialog(StorePanel.this, "You don't have enough coins!"
                            , Strings.ERROR, JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

}
