package ui.store;

import ui.CardsPanel;
import ui.card.CardType;
import ui.store.listeners.CardClickListener;

import javax.swing.*;
import java.awt.*;

public class StoreCardsRootPanel extends JPanel {
    CardsPanel buyCardPanel;
    CardsPanel sellCardPanel;
    CardLayout cardLayout;
    static boolean onBuy = true;
    StoreViewModel viewModel;

    public StoreCardsRootPanel() {
        super();

        viewModel = new StoreViewModel();
        
        cardLayout = new CardLayout();
        setLayout(cardLayout);
        setCardLayout();
    }

    public void setBuyCardClickListener(CardClickListener cardClickListener) {
        buyCardPanel.setCardClickListener(cardClickListener);
    }

    public void setSellCardClickListener(CardClickListener cardClickListener) {
        sellCardPanel.setCardClickListener(cardClickListener);
    }

    public void setCardLayout() {
        buyCardPanel = new CardsPanel(viewModel.getAvailableCards(), CardType.STORE);
        sellCardPanel = new CardsPanel(viewModel.getSellableCards(), CardType.STORE);
        JScrollPane buyCardScrollPane = new JScrollPane(buyCardPanel);
        JScrollPane sellCardScrollPane = new JScrollPane(sellCardPanel);
        buyCardScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        sellCardScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        add(buyCardScrollPane, "buy");
        add(sellCardScrollPane, "sell");
        if (onBuy) {
            switchToBuyPanel();
        } else {
            switchToSellPanel();
        }
    }

    public void switchToSellPanel() {
        cardLayout.show(this, "sell");
        onBuy = false;
    }

    public void switchToBuyPanel() {
        cardLayout.show(this, "buy");
        onBuy = true;
    }
}
