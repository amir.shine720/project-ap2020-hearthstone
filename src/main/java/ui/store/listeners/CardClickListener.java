package ui.store.listeners;

import cards.Card;

public interface CardClickListener {
    void cardClicked(Card card);

}
