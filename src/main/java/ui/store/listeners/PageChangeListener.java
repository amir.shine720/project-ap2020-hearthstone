package ui.store.listeners;

public interface PageChangeListener {
    void switchToSellPanel();
    void switchToBuyPanel();
    void back();
}
