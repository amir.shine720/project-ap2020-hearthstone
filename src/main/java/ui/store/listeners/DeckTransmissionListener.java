package ui.store.listeners;

import cards.Card;

public interface DeckTransmissionListener {
    public void addCardToDeck(Card card);
    public void removeCardFromDeck(Card card);
}
