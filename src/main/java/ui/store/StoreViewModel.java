package ui.store;

import cards.Card;
import data.Player;
import data.StoreDataModel;
import enums.Hero;
import utils.ImageGetter;
import utils.SellHeroCardException;

import javax.swing.*;
import java.util.ArrayList;

public class StoreViewModel {
    StoreDataModel dataModel = new StoreDataModel();

    public ArrayList<Card> getAvailableCards() {
        ArrayList<Card> availableCards = new ArrayList<>();
        for (Card card : dataModel.getHeroCards()) {
            if (!Player.getInstance().getCurrentHeroCards().contains(card)) {
                availableCards.add(card);
            }
        }
        for (Card card : dataModel.getHeroCards(Hero.NATURAL)) {
            if (!Player.getInstance().getCurrentHeroCards().contains(card)) {
                availableCards.add(card);
            }
        }
        return availableCards;
    }

    public boolean purchaseCard(Card card) {
        if (card.getValue() > Player.getInstance().getCoins()) {
            return false;
        }
        Player.getInstance().reduceCoins(card.getValue());
        Player.getInstance().addCard(card);
        return true;
    }

    public void sellCard(Card card) throws SellHeroCardException {
        if (card.getHero() != Hero.NATURAL) {
            throw new SellHeroCardException();
        }
        Player.getInstance().increaseCoins((int) (card.getValue() * 0.75));
        Player.getInstance().removeCard(card);
    }

    public ArrayList<Card> getSellableCards() {
        ArrayList<Card> sellableCards = new ArrayList<>();
        for (Card card : Player.getInstance().getCurrentHeroCards()) {
            if (card.getHero() == Hero.NATURAL) {
                sellableCards.add(card);
            }
        }
        return sellableCards;
    }

    public ImageIcon getCoinsIcon() {
        return ImageGetter.createImageIcon("src/main/resources/icons/coin.png", "coin icon");
    }

    public ImageIcon getManaIcon() {
        return ImageGetter.createImageIcon("src/main/resources/icons/mana_icon.png", "mana icon");
    }

    public ImageIcon getHealthIcon() {
        return ImageGetter.createImageIcon("src/main/resources/icons/health_icon.png", "health icon");
    }

    public ImageIcon getAttackIcon() {
        return ImageGetter.createImageIcon("src/main/resources/icons/attack_icon.png", "attack icon");
    }

    public int getPlayerCoins() {
        return Player.getInstance().getCoins();
    }
}
