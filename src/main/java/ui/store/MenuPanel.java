package ui.store;

import ui.store.listeners.PageChangeListener;

import javax.swing.*;
import java.awt.*;

public class MenuPanel extends JPanel {
    StoreViewModel viewModel;
    JLabel coinsLabel;
    JButton buyButton;
    JButton sellButton;
    JPanel coinsPanel;
    JButton backButton;

    PageChangeListener changeListener;

    public MenuPanel() {
        viewModel = new StoreViewModel();

        buyButton = new JButton("Buy");
        sellButton = new JButton("Sell");
        backButton = new JButton("Back");
        setCoinsPanel();

        setProperties();

        setListeners();

        addComponents();
    }

    private void setProperties() {
        buyButton.setBackground(Color.GRAY);
        sellButton.setFocusPainted(false);
        buyButton.setFocusPainted(false);
    }

    private void setListeners() {
        buyButton.addActionListener(actionEvent -> {
            sellButton.setBackground(null);
            buyButton.setBackground(Color.GRAY);
            changeListener.switchToBuyPanel();
        });
        sellButton.addActionListener(actionEvent -> {
            buyButton.setBackground(null);
            sellButton.setBackground(Color.GRAY);
            changeListener.switchToSellPanel();
        });
        backButton.addActionListener(actionEvent -> changeListener.back());
    }

    private void addComponents() {
        setLayout(new GridBagLayout());

        GridBagConstraints gc = new GridBagConstraints();

        gc.weightx = 0;
        gc.gridx = 0;

        gc.gridy = 0;
        gc.weighty = 0.5;
        gc.anchor = GridBagConstraints.CENTER;
        add(coinsPanel, gc);

        gc.gridy = 1;
        gc.weighty = 1;
        gc.insets = new Insets(0, 0, 5, 0);
        gc.anchor = GridBagConstraints.SOUTH;
        add(buyButton, gc);

        gc.gridy = 2;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.NORTH;
        add(sellButton, gc);

        gc.gridy = 2;
        gc.weighty = 8;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        add(backButton, gc);
    }

    private void setCoinsPanel() {
        coinsPanel = new JPanel();
        coinsLabel = new JLabel(String.valueOf(viewModel.getPlayerCoins()), viewModel.getCoinsIcon(), SwingConstants.CENTER);

        coinsPanel.setLayout(new FlowLayout());
        coinsPanel.add(new JLabel("Coins: "));
        coinsPanel.add(coinsLabel);
    }

    public void setChangeListener(PageChangeListener changeListener) {
        this.changeListener = changeListener;
    }

}
