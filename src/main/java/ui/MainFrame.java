package ui;

import data.CardProvider;
import enums.LogEvent;
import enums.Section;
import logger.MyLogger;
import ui.inventory.InventoryPanel;
import ui.login.LoginPanel;
import ui.play.PlayPanel;
import ui.settings.SettingsPanel;
import ui.status.StatusPanel;
import ui.store.StorePanel;
import ui.terminal.TerminalPanel;
import ui.terminal.TerminalViewModel;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class MainFrame extends JFrame {

    public MainFrame() {
        super("Hearthstone");
        CardProvider.initializeCards();
        CardsPanel.setAllCards();
        launchLogin();

        setExtendedState(Frame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setMinimumSize(new Dimension(1000, 800));
        setVisible(true);
    }

    public void launchLogin() {
        LoginPanel loginPanel = new LoginPanel();
        loginPanel.setNextSectionListener(section -> {
            loginPanel.setVisible(false);
            launchSection(section);
        });
        add(loginPanel);
    }

    private void launchTerminal() {
        MyLogger.log(LogEvent.NAVIGATE, "terminal");
        TerminalPanel terminalPanel = new TerminalPanel();
        terminalPanel.setNextSectionListener(section -> {
            terminalPanel.setVisible(false);
            launchSection(section);
        });
        add(terminalPanel);
    }

    private void launchPlay() {
        MyLogger.log(LogEvent.NAVIGATE, "play");
        PlayPanel playPanel = new PlayPanel();
        playPanel.setNextSectionListener(section -> {
            playPanel.setVisible(false);
            launchSection(section);
        });
        add(playPanel);

    }

    private void launchStore(){
        MyLogger.log(LogEvent.NAVIGATE, "store");
        StorePanel storePanel = new StorePanel();
        storePanel.setNextSectionListener(section -> {
            storePanel.setVisible(false);
            launchSection(section);
        });
        add(storePanel);
    }

    private void launchStatus() {
        MyLogger.log(LogEvent.NAVIGATE, "status");
        StatusPanel statusPanel = new StatusPanel();
        statusPanel.setNextSectionListener(section -> {
            statusPanel.setVisible(false);
            launchSection(section);
        });
        add(statusPanel);
    }

    private void launchInventory(){
        MyLogger.log(LogEvent.NAVIGATE, "collections");
        InventoryPanel inventoryPanel = new InventoryPanel();
        inventoryPanel.setNextSectionListener(section -> {
            inventoryPanel.setVisible(false);
            launchSection(section);
        });
        add(inventoryPanel);
    }

    private void launchSettings(){
        MyLogger.log(LogEvent.NAVIGATE, "settings");
        SettingsPanel settingsPanel = new SettingsPanel();
        settingsPanel.setNextSectionListener(section -> {
            settingsPanel.setVisible(false);
            launchSection(section);
        });
        add(settingsPanel);
    }

    private void launchExit() {
//        MyLogger.log(LogEvent.NAVIGATE, "exit");
        TerminalViewModel terminalViewModel = new TerminalViewModel();
        int result = JOptionPane.YES_OPTION;
        try {
            terminalViewModel.save();
        } catch (IOException e) {
            result = JOptionPane.showConfirmDialog(this, "We couldn't save your data!!\n" +
                    "Are you still wanna exit?","Save Data Error", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        }
        if(result == JOptionPane.YES_OPTION){
            System.exit(0);
        }
    }

    private void launchSection(Section section) {
        switch (section) {
            case LOGIN:
                launchLogin();
                break;
            case TERMINAL:
                launchTerminal();
                break;
            case PLAY:
                launchPlay();
                break;
            case STORE:
                launchStore();
                break;
            case STATUS:
                launchStatus();
                break;
            case INVENTORY:
                launchInventory();
                break;
            case SETTINGS:
                launchSettings();
                break;
            case EXIT:
                launchExit();
                break;
        }
    }
}
