package ui.settings;

import enums.Section;
import ui.BasicPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Arrays;

public class SettingsPanel extends BasicPanel {
    private JButton backButton;
    SettingsViewModel viewModel;
    JButton deleteAccountButton;

    public SettingsPanel() {
        super("Settings");

        viewModel = new SettingsViewModel();
        deleteAccountButton = new JButton("Delete Account");
        backButton = new JButton("Back");

        GridBagConstraints gc = new GridBagConstraints();
        gc.gridy = 0;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.SOUTH;
        add(deleteAccountButton, gc);

        gc.gridy = 1;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.FIRST_LINE_START;
        gc.insets = new Insets(20,0,10,0);
        add(backButton, gc);

        addListeners();
    }

    private void addListeners() {
        deleteAccountButton.addActionListener(actionEvent -> {
            String password = JOptionPane.showInputDialog(SettingsPanel.this.getRootPane(), "You are about to delete your" +
                    " account, it means all your data and progress will delete!\n" +
                    "If you are sure, enter your password and proceed.", "Delete Account", JOptionPane.WARNING_MESSAGE);
            if (password != null) {
                boolean result = false;
                try {
                    result = viewModel.deleteAccount(Arrays.toString(password.toCharArray()));
                } catch (IOException e) {
                    JOptionPane.showMessageDialog(SettingsPanel.this.getRootPane(), "Something went wrong!\n" +
                            "Please try again later");
                }
                if (!result) {
                    JOptionPane.showMessageDialog(SettingsPanel.this.getRootPane(), "Wrong Password!");
                }else {
                    nextSectionListener.nextSectionRequired(Section.LOGIN);
                }
            }
        });

        backButton.addActionListener(actionEvent -> nextSectionListener.nextSectionRequired(Section.TERMINAL));
    }
}
