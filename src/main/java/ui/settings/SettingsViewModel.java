package ui.settings;

import data.Player;
import enums.LogEvent;
import logger.MyLogger;
import ui.data.SettingsDataModel;

import java.io.IOException;

public class SettingsViewModel {

    public boolean deleteAccount(String password) throws IOException {
        SettingsDataModel dataModel= new SettingsDataModel();
        if (!Player.getInstance().getPassword().equals(password)) {
            return false;
        }
        dataModel.deletePlayer();

        MyLogger.log(LogEvent.LOGOUT, Player.getInstance().getUsername());
        MyLogger.log(LogEvent.DELETE_ACCOUNT, Player.getInstance().getUsername());
        MyLogger.deletePlayer();
        Player.getInstance().clear();
        return true;
    }
}
