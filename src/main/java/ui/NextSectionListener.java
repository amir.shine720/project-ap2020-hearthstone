package ui;

import enums.Section;

import java.util.EventListener;

public interface NextSectionListener extends EventListener {
    void nextSectionRequired(Section section);
}
