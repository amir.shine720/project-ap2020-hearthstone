package ui.terminal;

import enums.LogEvent;
import enums.Section;
import logger.MyLogger;
import ui.BasicPanel;

import javax.swing.*;
import java.awt.*;

public class TerminalPanel extends BasicPanel {
    JButton playButton;
    JButton storeButton;
    JButton statusButton;
    JButton collectionsButton;
    JButton settingsButton;

    public TerminalPanel() {
        super("Terminal");

        setLayout(new BorderLayout());

        initComponents();

        setProperties();

        addListeners();

        addComponents();
    }

    private void addComponents() {
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        gc.gridx = 0;
        gc.weightx = 0;

        gc.gridy = 0;
        gc.weighty = 4;
        gc.anchor = GridBagConstraints.CENTER;
        menuPanel.add(new JLabel("Welcome to Terminal"), gc);

        gc.gridy++;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.CENTER;
        menuPanel.add(playButton, gc);

        gc.gridy++;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.CENTER;
        menuPanel.add(storeButton, gc);

        gc.gridy++;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.CENTER;
        menuPanel.add(statusButton, gc);

        gc.gridy++;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.CENTER;
        menuPanel.add(collectionsButton, gc);

        gc.gridy++;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.CENTER;
        menuPanel.add(settingsButton, gc);

        gc.gridy++;
        gc.weighty = 4;
        menuPanel.add(new JLabel(), gc);


        JPanel exitPanel = new JPanel();
        exitPanel.setLayout(new GridBagLayout());
        gc.gridy = 0;
        gc.gridx = 0;
        gc.anchor = GridBagConstraints.FIRST_LINE_END;
        gc.insets = new Insets(10, 10, 10, 10);
        Dimension exitButtonSize = exitButton.getPreferredSize();
        exitButtonSize.height = exitButtonSize.height * 2;
        exitButtonSize.width = exitButtonSize.width * 2;
        exitButton.setPreferredSize(exitButtonSize);
        exitButton.setBackground(Color.RED);
        exitPanel.add(exitButton, gc);

        add(menuPanel, BorderLayout.CENTER);
        add(exitPanel, BorderLayout.EAST);
    }

    private void addListeners() {
        playButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "play button");
            nextSectionListener.nextSectionRequired(Section.PLAY);
        });
        statusButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "status button");
            nextSectionListener.nextSectionRequired(Section.STATUS);
        });
        storeButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "store button");
            nextSectionListener.nextSectionRequired(Section.STORE);
        });
        collectionsButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "collections button");
            nextSectionListener.nextSectionRequired(Section.INVENTORY);
        });
        settingsButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "settings button");
            nextSectionListener.nextSectionRequired(Section.SETTINGS);
        });
    }

    private void setProperties() {
        playButton.setPreferredSize(new Dimension(150, 50));
        statusButton.setPreferredSize(new Dimension(150, 50));
        storeButton.setPreferredSize(new Dimension(150, 50));
        collectionsButton.setPreferredSize(new Dimension(150, 50));
        settingsButton.setPreferredSize(new Dimension(150, 50));
    }

    private void initComponents() {
        playButton = new JButton("Play");
        storeButton = new JButton("Store");
        statusButton = new JButton("Status");
        collectionsButton = new JButton("Collections");
        settingsButton = new JButton("Settings");
    }

}
