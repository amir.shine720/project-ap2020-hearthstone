package ui.terminal;

import cards.Card;
import data.Player;
import data.TerminalDataModel;
import enums.LogEvent;
import logger.MyLogger;
import ui.login.LoginViewModel;

import java.io.IOException;
import java.util.List;

public class TerminalViewModel {
    TerminalDataModel dataModel = new TerminalDataModel();

    public List<Card> getAllCards() {
        return dataModel.getAllCards();
    }

    public void save() throws IOException {
        LoginViewModel loginViewModel = new LoginViewModel();
        loginViewModel.savePlayerData();

    }

    public void logOut() throws IOException {
        save();
        MyLogger.log(LogEvent.LOGOUT, Player.getInstance().getUsername());
        Player.getInstance().clear();
    }
}