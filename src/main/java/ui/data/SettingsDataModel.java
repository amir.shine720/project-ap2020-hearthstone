package ui.data;

import data.Player;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class SettingsDataModel {
    public void deletePlayer() throws IOException {
        File file =new File(String.format("src/main/resources/players/%s.json", Player.getInstance().getUsername()));
        Files.delete(file.toPath());
    }

}
