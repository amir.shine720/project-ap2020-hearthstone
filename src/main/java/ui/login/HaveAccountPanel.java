package ui.login;

import ui.login.listeners.LoginListener;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class HaveAccountPanel extends JPanel {

    private JButton signUpButton;
    private JButton signInButton;
    private JButton exitButton;
    private LoginListener loginListener;

    public HaveAccountPanel() {
        Dimension dim = getPreferredSize();
        dim.width = 250;
        setPreferredSize(dim);

        initializeComponents();

        setProperties();

        addListeners();

        addComponents();

    }

    private void initializeComponents() {
        signUpButton = new JButton("Sign Up");
        signInButton = new JButton("Sign In");
        exitButton = new JButton("Exit");
    }

    private void setProperties() {
        signUpButton.setAlignmentX(CENTER_ALIGNMENT);
        signInButton.setAlignmentX(CENTER_ALIGNMENT);
        exitButton.setAlignmentX(LEFT_ALIGNMENT);
        Dimension dimension = signUpButton.getPreferredSize();
        dimension.width += 10;
        dimension.height += 10;
        signUpButton.setPreferredSize(dimension);
        signInButton.setPreferredSize(dimension);
    }

    private void addListeners() {
        signUpButton.addActionListener(actionEvent -> loginListener.signUpEventOccurred());
        signInButton.addActionListener(actionEvent -> loginListener.signInEventOccurred());
        exitButton.addActionListener(actionEvent -> loginListener.exitEventOccurred());
    }

    public void setActionListener(LoginListener loginListener) {
        this.loginListener = loginListener;
    }

    private void addComponents() {
        setLayout(new GridBagLayout());

        GridBagConstraints gc = new GridBagConstraints();
        gc.weightx = 1;
        gc.insets = new Insets(20, 20, 20, 20);

        gc.weighty = 4;
        gc.gridy = 0;
        gc.anchor = GridBagConstraints.SOUTH;
        add(new JLabel("Welcome to Hearthstone"), gc);

        gc.weighty = 2;
        gc.gridy = 1;
        gc.anchor = GridBagConstraints.CENTER;
        add(signUpButton, gc);

        gc.weighty = 4;
        gc.gridy = 2;
        gc.anchor = GridBagConstraints.NORTH;
        add(signInButton, gc);

        gc.weighty = 1;
        gc.gridy = 3;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        add(exitButton, gc);
    }
}