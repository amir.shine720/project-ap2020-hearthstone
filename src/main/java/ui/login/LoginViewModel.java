package ui.login;

import data.CardProvider;
import data.LoginDataModel;
import data.Player;
import enums.Hero;
import enums.LogEvent;
import logger.MyLogger;
import ui.terminal.TerminalViewModel;
import utils.Password;

import java.io.IOException;

public class LoginViewModel {
    LoginDataModel dataModel = new LoginDataModel();

    public boolean addPlayer(String username, String password) throws IOException {
        Player.getInstance().initialize(username, Password.encrypt(password), CardProvider.getDefaultDecks(),
                CardProvider.getDefaultCards(), Hero.DEFAULT_HERO, 50, null);

        boolean result = dataModel.addPlayer();
        if (!result) {
            Player.getInstance().clear();
        } else {
            MyLogger.log(LogEvent.SIGNIN, Player.getInstance().getUsername());
        }
        return result;
    }

    public boolean loginPlayer(String username, String password) {
        Player playerData = LoginDataModel.getPlayerData(username);
        if (playerData == null) {
            return false;
        }

        Player.getInstance().initialize(playerData);
        boolean result = Player.getInstance().getPassword().equals(password);
        if (result) {
            MyLogger.log(LogEvent.SIGNIN, Player.getInstance().getUsername());
        }
        return result;
    }

    public void savePlayerData() throws IOException {
        if(Player.getInstance().getUsername() == null){
            return;
        }
        dataModel.savePlayerData();
    }
}
