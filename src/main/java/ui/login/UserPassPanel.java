package ui.login;

import ui.login.listeners.BackListener;
import ui.login.listeners.UserPassListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class UserPassPanel extends JPanel {

    JTextField usernameTextField;
    JPasswordField passwordTextField;
    JButton okButton;
    JButton backButton;
    JButton exitButton;
    UserPassListener userPassListener;
    BackListener backListener;

    public UserPassPanel() {
        Dimension dimension = getPreferredSize();
        dimension.width = 250;
        setPreferredSize(dimension);

        usernameTextField = new JTextField(10);
        passwordTextField = new JPasswordField(10);
        okButton = new JButton("Submit");
        backButton = new JButton("Back");
        exitButton = new JButton("Exit");

        dimension = okButton.getPreferredSize();
        dimension.width += 5;
        dimension.height += 5;
        okButton.setPreferredSize(dimension);
        backButton.setPreferredSize(dimension);

        setListeners();

        setLayout(new GridBagLayout());

        GridBagConstraints gc = new GridBagConstraints();


        gc.weightx = 1;
        gc.weighty = 2;

        gc.gridy = 1;
        gc.gridx = 0;
        gc.insets = new Insets(0,0,0,5);
        gc.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Username:"), gc);

        gc.gridy = 1;
        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(usernameTextField, gc);


        gc.weightx = 1;
        gc.weighty = 2;

        gc.gridy = 2;
        gc.gridx = 0;
        gc.insets = new Insets(0,0,0,5);
        gc.anchor = GridBagConstraints.LINE_END;
        add(new JLabel("Password:"), gc);


        gc.gridy = 2;
        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(passwordTextField, gc);

        gc.weighty = 6;

        gc.gridy = 3;
        gc.gridx = 1;
        gc.insets = new Insets(0,0,5,0);
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        add(okButton, gc);

        gc.weightx = 1;
        gc.weighty = 1;

        gc.gridy = 4;
        gc.gridx = 1;
        gc.anchor = GridBagConstraints.LINE_START;
        add(backButton, gc);

        gc.weighty = 1;
        gc.gridx = 0;
        gc.gridy = 5;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        add(exitButton, gc);
    }

    private void setListeners() {
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String username = usernameTextField.getText();
                String password = Arrays.toString(passwordTextField.getPassword());

                UserPassEvent event = new UserPassEvent(this, username, password);

                if(userPassListener != null) {
                    userPassListener.userPassReceived(event);
                }
            }
        });

        backButton.addActionListener(actionEvent -> backListener.backPressed());
        exitButton.addActionListener(actionEvent -> userPassListener.exitRequested());
//
    }

    public void focusUsername(){
        usernameTextField.requestFocusInWindow();

    }

    public void setEnterToSubmit() {
        getRootPane().setDefaultButton(okButton);
    }

    public void setUserPassListener(UserPassListener userPassListener){
        this.userPassListener = userPassListener;
    }
    public void setBackListener(BackListener backListener){
        this.backListener = backListener;
    }

    public void clearTextFields() {
        usernameTextField.setText("");
        passwordTextField.setText("");
    }
}
