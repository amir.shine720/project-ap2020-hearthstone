package ui.login.listeners;

import java.util.EventListener;

public interface LoginListener extends EventListener {
    void signInEventOccurred();
    void signUpEventOccurred();
    void exitEventOccurred();
}
