package ui.login.listeners;

import ui.login.UserPassEvent;

public interface UserPassListener {
    void userPassReceived(UserPassEvent userPassEvent);

    void exitRequested();
}
