package ui.login;

import data.Player;
import enums.LogEvent;
import enums.LoginOption;
import enums.Section;
import logger.MyLogger;
import ui.BasicPanel;
import ui.login.listeners.LoginListener;
import ui.login.listeners.UserPassListener;
import utils.Strings;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class LoginPanel extends BasicPanel {

    private HaveAccountPanel haveAccountPanel;
    private UserPassPanel userPassPanel;
    private LoginOption loginOption;
    private LoginViewModel viewModel;

    public LoginPanel() {
        super("Login");

        viewModel = new LoginViewModel();

        setLayout(new GridBagLayout());

        haveAccountPanel = new HaveAccountPanel();
        userPassPanel = new UserPassPanel();

        userPassPanel.setVisible(false);

        setListeners();

        addComponents();

        setSize(400, 300);

        setVisible(true);

    }

    private void addComponents() {
        add(haveAccountPanel, new GridBagConstraints(
                0, 0,
                0, 0,
                0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 80, 400));

        add(userPassPanel, new GridBagConstraints(
                0, 0,
                0, 0,
                0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 80, 400));
    }

    private void setListeners() {
        haveAccountPanel.setActionListener(new LoginListener() {
            @Override
            public void signInEventOccurred() {
                launchUserPassPanel();
                loginOption = LoginOption.SIGN_IN;
            }

            @Override
            public void signUpEventOccurred() {
                launchUserPassPanel();
                loginOption = LoginOption.SIGN_UP;
            }

            @Override
            public void exitEventOccurred() {
                nextSectionListener.nextSectionRequired(Section.EXIT);
            }
        });

        userPassPanel.setUserPassListener(new UserPassListener() {
            @Override
            public void userPassReceived(UserPassEvent userPassEvent) {
                String username = userPassEvent.getUsername();
                String password = userPassEvent.getPassword();
                if(username.isEmpty() || password.isEmpty()){
                    JOptionPane.showMessageDialog(LoginPanel.this,
                            "Please Enter Your Username And Password First","Empty User Pass", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                switch (loginOption) {
                    case SIGN_IN:
                        loginAccount(username, password);
                        break;
                    case SIGN_UP:
                        createAccount(username, password);
                        break;
                }
            }

            @Override
            public void exitRequested() {
                nextSectionListener.nextSectionRequired(Section.EXIT);
            }

        });

        userPassPanel.setBackListener(this::launchHaveAccountPanel);
    }

    private void launchUserPassPanel() {
        haveAccountPanel.setVisible(false);
        userPassPanel.setVisible(true);
        userPassPanel.focusUsername();
        userPassPanel.setEnterToSubmit();
    }

    private void launchHaveAccountPanel() {
        userPassPanel.setVisible(false);
        userPassPanel.clearTextFields();
        haveAccountPanel.setVisible(true);
    }

    private void createAccount(String username, String password) {
        try {
            boolean result = viewModel.addPlayer(username, password);
            if (result) {
                MyLogger.log(LogEvent.SIGNIN, Player.getInstance().getUsername());
                nextSectionListener.nextSectionRequired(Section.TERMINAL);
            } else {
                JOptionPane.showMessageDialog(LoginPanel.this,
                        "Username already exists!\nPlease use another one:",
                        Strings.ERROR, JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException e) {
            MyLogger.log(LogEvent.ERROR, e.getMessage());
            JOptionPane.showMessageDialog(LoginPanel.this,
                    "Something went wrong!\n Please try again...",
                    Strings.ERROR, JOptionPane.ERROR_MESSAGE);
        }
    }

    private void loginAccount(String username, String password) {
        boolean result = viewModel.loginPlayer(username, password);
        if (result) {
            nextSectionListener.nextSectionRequired(Section.TERMINAL);
        } else {
            JOptionPane.showMessageDialog(LoginPanel.this,
                    "Incorrect Username or Password!\nPlease try again",
                    Strings.ERROR, JOptionPane.ERROR_MESSAGE);
        }
    }
}