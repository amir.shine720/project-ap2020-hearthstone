package ui.login;

import java.util.EventObject;

public class UserPassEvent extends EventObject {
    String username;
    String password;

    public UserPassEvent(Object source, String username, String password) {
        super(source);
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
