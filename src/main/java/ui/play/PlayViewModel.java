package ui.play;

import cards.Card;
import data.PlayDataModel;
import data.Player;
import enums.Hero;
import enums.Type;
import ui.store.StoreViewModel;

import javax.swing.*;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayViewModel {
    private GamePlayer currentPlayer;
    private GamePlayer otherPlayer;

    public PlayViewModel() {
        this.currentPlayer = new GamePlayer();
        this.otherPlayer = new GamePlayer();

        currentPlayer.setUsername(Player.getInstance().getUsername());
        currentPlayer.setDeck(Player.getInstance().getCurrentDeck());
        currentPlayer.setHero(Player.getInstance().getHero());
        if(currentPlayer.getHero() == Hero.WARLOCK){
            currentPlayer.setHealth(35);
        }
        currentPlayer.setInitialCards();

        otherPlayer.setUsername(Player.getInstance().getUsername());
        otherPlayer.setDeck(Player.getInstance().getCurrentDeck());
        otherPlayer.setHero(Player.getInstance().getHero());
        if(otherPlayer.getHero() == Hero.WARLOCK){
            otherPlayer.setHealth(35);
        }
        otherPlayer.setInitialCards();

    }

    public ArrayList<Card> getCurrentPlayerPlayedCards() {
        return currentPlayer.getPlayedCards();
    }

    public ArrayList<Card> getCurrentPlayerHeldCards() {
        return currentPlayer.getHeldCards();
    }

    public int getCurrentPlayerMana() {
        return currentPlayer.getMana();
    }

    public ArrayList<Card> getOtherPlayerPlayedCards() {
        return otherPlayer.getPlayedCards();
    }

    public ArrayList<Card> getOtherPlayerHeldCards() {
        return otherPlayer.getHeldCards();
    }

    public int getOtherPlayerMana() {
        return otherPlayer.getMana();
    }

    public GamePlayer getCurrentPlayer() {
        return currentPlayer;
    }

    public GamePlayer getOtherPlayer() {
        return otherPlayer;
    }

    public void addRandomCardCurrentPlayerHeld() {
        if (currentPlayer.getHeldCards().size() == 12) {
            return;
        }
        currentPlayer.addToHeldCards(getRandomCardFromDeck());
    }

    Card getRandomCardFromDeck() {
        SecureRandom random = new SecureRandom();
        List<Card> cards = currentPlayer.getDeck().getCards();
        return cards.get(random.nextInt(cards.size()));
    }

    public void increaseCurrentPlayerManaCap() {
        if (currentPlayer.getManaCap() == 10) {
            return;
        }
        currentPlayer.increaseManaCap(1);
    }

    public void reduceCurrentPlayerMana(int amount) {
        currentPlayer.reduceMana(amount);
    }

    public void addCardCurrentPlayerPlayed(Card card) {
        currentPlayer.addToPlayedCard(card);
    }

    public void removeCardCurrentPlayerHeld(Card card) {
        currentPlayer.removeFromHeldCards(card);
    }

    public void setCurrentPlayerMana(int mana) {
        this.currentPlayer.setMana(mana);
    }

    public int getCurrentPlayerManaCap() {
        return currentPlayer.getManaCap();
    }

    public ArrayList<Card> getRandomPassiveCards() throws IOException {
        PlayDataModel dataModel = new PlayDataModel();
        ArrayList<Card> passiveCards = dataModel.getPassiveCards();
        Collections.shuffle(passiveCards);

        ArrayList<Card> randomCards = new ArrayList<>();
        randomCards.add(passiveCards.get(0));
        randomCards.add(passiveCards.get(1));
        randomCards.add(passiveCards.get(2));
        return randomCards;
    }

    public void setPassiveCard(Card card) {
        currentPlayer.setPassiveCard(card);
    }

    public ImageIcon getHealthIcon() {
        StoreViewModel storeViewModel = new StoreViewModel();
        return storeViewModel.getHealthIcon();
    }

    public ImageIcon getHeroImage(Hero hero) {
        PlayDataModel dataModel = new PlayDataModel();
        return dataModel.getHeroImage(hero);
    }

    public void endCurrentPlayerTurn() {
        addRandomCardCurrentPlayerHeld();
        increaseCurrentPlayerManaCap();
        setCurrentPlayerMana(getCurrentPlayerManaCap());
    }

    public void playCurrentPlayerCard(Card card) {
        if(card.getType() == Type.MINION) {
            addCardCurrentPlayerPlayed(card);
        }
        removeCardCurrentPlayerHeld(card);
        reduceCurrentPlayerMana(card.getManaCost());
    }
}
