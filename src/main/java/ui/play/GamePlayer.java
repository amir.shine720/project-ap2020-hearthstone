package ui.play;

import cards.Card;
import cards.Deck;
import data.Player;
import enums.Hero;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GamePlayer {
    private ArrayList<Card> playedCards;
    private ArrayList<Card> heldCards;
    private Deck deck;
    private int mana;
    private int manaCap;
    private Hero hero;
    private int health;

    private Card passiveCard;
    private String username;

    public GamePlayer() {
        playedCards = new ArrayList<>();
        heldCards = new ArrayList<>();
        mana = 1;
        manaCap = 1;
        health = 30;
    }

    public int getManaCap() {
        return manaCap;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public ArrayList<Card> getPlayedCards() {
        return playedCards;
    }

    public void addToPlayedCard(Card card) {
        playedCards.add(card);
    }

    public void removeFromPlayedCard(Card card) {
        playedCards.remove(card);
    }

    public ArrayList<Card> getHeldCards() {
        return heldCards;
    }

    public void addToHeldCards(Card card) {
        heldCards.add(card);
    }

    public void removeFromHeldCards(Card card) {
        heldCards.remove(card);
    }

    public int getMana() {
        return mana;
    }

    public void increaseManaCap(int amount) {
        manaCap += amount;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void reduceMana(int i) {
        mana -= i;
    }

    public void setPassiveCard(Card card) {
        passiveCard = card;
    }

    public Card getPassiveCard() {
        return passiveCard;
    }

    public String getUserName() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setInitialCards() {
        List<Card> cards = Player.getInstance().getCurrentDeck().getCards();
        Collections.shuffle(cards);
        for (int i = 0; i < 3; i++) {
            addToHeldCards(cards.get(i));
        }
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
