package ui.play;

import cards.Card;
import ui.card.CardLabel;
import ui.store.StoreViewModel;

import javax.swing.*;
import java.util.ArrayList;

public class PlayCardLabel extends CardLabel {
    public PlayCardLabel(Card card) {
        super(card);
    }

    @Override
    protected void initRequiredComponents(Card card) {
        StoreViewModel viewModel = new StoreViewModel();
        cardNameLabel = new JLabel(card.getName());
        cardManaLabel = new JLabel(String.valueOf(card.getManaCost()),
                viewModel.getManaIcon(), SwingConstants.CENTER);
        cardHealthLabel = new JLabel(String.valueOf(card.getHealth()),
                viewModel.getHealthIcon(), SwingConstants.CENTER);
        cardAttackLabel = new JLabel(String.valueOf(card.getAttack()),
                viewModel.getAttackIcon(), SwingConstants.CENTER);
        cardDescriptionLabel = new JTextArea(card.getDescription());
    }

    @Override
    protected void fillSatComponentsIntoArray(ArrayList<JComponent> components) {
        components.add(cardNameLabel);
        components.add(cardManaLabel);
        components.add(cardHealthLabel);
        components.add(cardAttackLabel);
        components.add(cardDescriptionLabel);

    }
}
