package ui.play;

import data.Player;
import ui.store.StoreViewModel;

import javax.swing.*;
import java.awt.*;

public class PlayerStatusPanel extends JPanel {

    static Dimension initialSize;

    public PlayerStatusPanel(GamePlayer player) {
        super();

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder("Player Status"));

        GridBagConstraints gc = new GridBagConstraints();

        gc.gridx = 0;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.CENTER;

        gc.gridy = 0;
        add(new JLabel(player.getUserName()), gc);

        gc.gridy++;
        add(new JLabel(player.getDeck().getHero().getName()), gc);

        if (player.getPassiveCard() != null) {
            gc.gridy++;
            add(new JLabel(player.getPassiveCard().getDescription()), gc);
        }

        gc.gridy++;
        StoreViewModel storeViewModel = new StoreViewModel();
        add(new JLabel(String.format("%d/%d", player.getMana(), player.getManaCap())
                , storeViewModel.getManaIcon(), SwingConstants.CENTER), gc);
    }
}
