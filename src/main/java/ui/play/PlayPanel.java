package ui.play;

import enums.LogEvent;
import enums.Section;
import enums.Type;
import logger.MyLogger;
import ui.BasicPanel;
import ui.CardsPanel;
import ui.card.CardType;

import javax.swing.*;
import java.awt.*;

public class PlayPanel extends BasicPanel {
    PassiveFrame passiveFrame;
    CardsPanel currentPlayerPlayground;
    CardsPanel otherPlayerPlayground;
    CardsPanel currentPlayerHeldCards;
    CardsPanel otherPlayerHeldCards;

    PlayerStatusPanel currentPlayerStatusPanel;
    PlayerStatusPanel otherPlayerStatusPanel;
    HeroPlayPanel currentHeroPlayPanel;
    HeroPlayPanel otherHeroPlayPanel;
    JButton endTurnButton;
    JButton backButton;
    PlayViewModel viewModel;
    boolean currentPlayerTurn;

    public PlayPanel() {
        super("Play");

        viewModel = new PlayViewModel();
        initializeComponents();

        currentPlayerTurn = true;

        addListener();

        addComponents();

        showChoosePassiveFrame();
    }

    private void showChoosePassiveFrame() {
        passiveFrame = new PassiveFrame();
        passiveFrame.setCardClickListener(card -> {
            viewModel.setPassiveCard(card);
            passiveFrame.setVisible(false);
            refresh();
        });

        passiveFrame.setVisible(true);
    }

    private void addComponents() {
        setLayout(new GridLayout(2, 1));

        JScrollPane scrollPane = getScrollPane(getOtherPlayerPanel());
        scrollPane.setAlignmentX(CENTER_ALIGNMENT);
        add(getOtherPlayerPanel());

        endTurnButton.setAlignmentX(RIGHT_ALIGNMENT);

        scrollPane = getScrollPane(getCurrentPlayerPanel());
        scrollPane.setAlignmentX(CENTER_ALIGNMENT);
        add(getCurrentPlayerPanel());
    }

    private void addListener() {

        endTurnButton.addActionListener(actionEvent -> {
            if (currentPlayerTurn) {
                MyLogger.log(LogEvent.CLICK_BUTTON, "end turn button");
                if (viewModel.getCurrentPlayerHeldCards().size() == 12) {
                    showLoseCardDialog();
                }

                viewModel.endCurrentPlayerTurn();

                refresh();
            }
        });

        currentPlayerHeldCards.setCardClickListener(card -> {
            MyLogger.log(LogEvent.CLICK_CARD, "played card");
            if (viewModel.getCurrentPlayerMana() < card.getManaCost()) {
                MyLogger.log(LogEvent.CLICK_CARD, "didn't have enough mana");
                JOptionPane.showMessageDialog(this, "Not Enough Mana!");
                return;
            }
            if(viewModel.getCurrentPlayerPlayedCards().size() == 7 && card.getType() == Type.MINION){
                MyLogger.log(LogEvent.CLICK_CARD, "played card limit reached");
                JOptionPane.showMessageDialog(this, "Not Enough Space To Play This Card!!");
                return;
            }

            viewModel.playCurrentPlayerCard(card);
            refresh();
        });

        backButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "back button");

            int youAreSure = JOptionPane.showConfirmDialog(PlayPanel.this,
                    "You are about to abandon a match, therefore there will be some penalty(lol)!!!\nAre you sure?",
                    "Exit Match", JOptionPane.YES_NO_OPTION);
            if (youAreSure == JOptionPane.YES_OPTION) {
                MyLogger.log(LogEvent.ABANDON, "abandoned match");
                nextSectionListener.nextSectionRequired(Section.TERMINAL);
            }
        });
    }

    private void showLoseCardDialog() {
        JDialog dialog = new JDialog();
        dialog.setTitle("Full Hand");
        dialog.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        gc.gridy = 0;
        gc.anchor = GridBagConstraints.CENTER;
        gc.insets = new Insets(5,0,5,0);

        gc.weighty = 1;
        dialog.add(new JLabel("Sorry, your hand is full, so you'll lose this card: "), gc);

        gc.gridy = 1;
        gc.weighty = 4;
        gc.fill= GridBagConstraints.BOTH;
        dialog.add(new PlayCardLabel(viewModel.getRandomCardFromDeck()), gc);

        gc.gridy = 2;
        gc.weighty = 1;
        gc.fill= GridBagConstraints.NONE;
        JButton okButton = new JButton("ok shame!");
        okButton.addActionListener(actionEvent1 -> dialog.setVisible(false));
        dialog.add(okButton, gc);

        dialog.setPreferredSize(new Dimension(300,400));
        dialog.pack();
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

    private void initializeComponents() {

        currentPlayerPlayground = new CardsPanel(viewModel.getCurrentPlayerPlayedCards(), 7, CardType.PLAY);
        currentPlayerHeldCards = new CardsPanel(viewModel.getCurrentPlayerHeldCards(), 12, CardType.PLAY);
        otherPlayerPlayground = new CardsPanel(viewModel.getOtherPlayerPlayedCards(), 7, CardType.PLAY);
        otherPlayerHeldCards = new CardsPanel(viewModel.getOtherPlayerHeldCards(), 12, CardType.PLAY);

        currentPlayerStatusPanel = new PlayerStatusPanel(viewModel.getCurrentPlayer());
        currentHeroPlayPanel = new HeroPlayPanel(viewModel.getCurrentPlayer());
        otherPlayerStatusPanel = new PlayerStatusPanel(viewModel.getOtherPlayer());
        otherHeroPlayPanel = new HeroPlayPanel(viewModel.getOtherPlayer());

        endTurnButton = new JButton("End Turn");
        backButton = new JButton("Back");

    }

    private JPanel getCurrentPlayerPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        JPanel playGroundPanel = new JPanel(new GridLayout(2, 1));
        playGroundPanel.add(getScrollPane(currentPlayerPlayground));
        playGroundPanel.add(getScrollPane(currentPlayerHeldCards));

        gc.gridx = 0;
        gc.gridy = 0;
        gc.weightx = 0.1;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        panel.add(backButton, gc);

        gc.fill = GridBagConstraints.BOTH;

        gc.gridx = 1;
        gc.gridy = 0;
        gc.weightx = 9;
        gc.weighty = 5;
        panel.add(playGroundPanel, gc);

        JPanel statusPanel = new JPanel(new GridBagLayout());
        gc.anchor = GridBagConstraints.CENTER;

        gc.gridx = 1;
        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = 0.1;
        gc.fill = GridBagConstraints.NONE;
        statusPanel.add(endTurnButton, gc);

        gc.gridx = 1;
        gc.gridy = 1;
        gc.weightx = 1;
        gc.weighty = 2.5;
        statusPanel.add(currentHeroPlayPanel, gc);

        gc.gridx = 1;
        gc.gridy = 2;
        gc.weightx = 1;
        gc.weighty = 2.5;
        gc.fill = GridBagConstraints.BOTH;
        statusPanel.add(currentPlayerStatusPanel, gc);

        gc.gridx = 2;
        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = 1;
        panel.add(statusPanel, gc);

        return panel;
    }

    private JPanel getOtherPlayerPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        JPanel playGroundPanel = new JPanel(new GridLayout(2, 1));
        playGroundPanel.add(getScrollPane(otherPlayerHeldCards));
        playGroundPanel.add(getScrollPane(otherPlayerPlayground));

        gc.gridx = 0;
        gc.gridy = 0;
        gc.weightx = 0.1;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        JLabel emptyLabel = new JLabel();
        emptyLabel.setPreferredSize(new Dimension(55, 0));
        panel.add(emptyLabel, gc);

        gc.fill = GridBagConstraints.BOTH;

        gc.gridx = 1;
        gc.gridy = 0;
        gc.weightx = 9;
        gc.weighty = 5;
        panel.add(playGroundPanel, gc);

        JPanel helperPanel = new JPanel(new GridBagLayout());
        gc.anchor = GridBagConstraints.CENTER;

        gc.gridx = 1;
        gc.gridy = 0;
        gc.weightx = 1;
        gc.weighty = 1;
        helperPanel.add(otherPlayerStatusPanel, gc);

        gc.fill = GridBagConstraints.NONE;
        gc.gridx = 1;
        gc.gridy = 1;
        gc.weightx = 1;
        gc.weighty = 1;
        helperPanel.add(otherHeroPlayPanel, gc);

        gc.fill = GridBagConstraints.BOTH;
        gc.gridy = 0;
        gc.gridx = 2;
        gc.weightx = 1;
        panel.add(helperPanel, gc);

        return panel;
    }

    private void refresh() {
        removeAll();
        revalidate();
        repaint();
        initializeComponents();
        addComponents();
        addListener();
    }

    private JScrollPane getScrollPane(JPanel panel) {
        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        return scrollPane;
    }
}
