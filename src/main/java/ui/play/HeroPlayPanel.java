package ui.play;

import javax.swing.*;
import java.awt.*;

public class HeroPlayPanel extends JPanel {
    PlayViewModel viewModel;

    public HeroPlayPanel(GamePlayer player) {

        viewModel = new PlayViewModel();

        setLayout(new GridBagLayout());

        GridBagConstraints gc = new GridBagConstraints();

        gc.insets = new Insets(5,5,5,5);

        gc.gridx = 1;
        gc.gridy = 0;
        gc.weightx = 5;
        gc.weighty = 1;
        add(new JLabel(player.getHero().getName()), gc);

        gc.gridx = 1;
        gc.gridy = 1;
        gc.weightx = 5;
        gc.weighty = 5;
        add(new JLabel(viewModel.getHeroImage(player.getHero())), gc);


        gc.gridx = 1;
        gc.gridy = 2;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.CENTER;
        add(new JLabel(player.getHero().getSpecialPower().getDescription()), gc);

        gc.gridx = 1;
        gc.gridy = 3;
        gc.weightx = 1;
        gc.weighty = 1;
        gc.anchor = GridBagConstraints.LAST_LINE_START;
        add(new JLabel(String.valueOf(player.getHealth()), viewModel.getHealthIcon(), SwingConstants.CENTER), gc);
    }
}
