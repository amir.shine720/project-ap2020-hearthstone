package ui.play;

import enums.LogEvent;
import logger.MyLogger;
import ui.CardsPanel;
import ui.card.CardType;
import ui.store.listeners.CardClickListener;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class PassiveFrame extends JDialog {
    PlayViewModel viewModel;
    CardsPanel cardsPanel;

    public PassiveFrame() {
        super();
        viewModel = new PlayViewModel();
        setTitle("PassiveCards");
        setLayout(new BorderLayout());

        addComponents();
        pack();
        setLocationRelativeTo(null);
    }

    private void addComponents() {
        JPanel rootPanel = new JPanel();
        rootPanel.setBorder(BorderFactory.createTitledBorder("Select a Passive Card"));
        try {
            cardsPanel = new CardsPanel(viewModel.getRandomPassiveCards(), 3, "", CardType.PASSIVE);
            rootPanel.add(cardsPanel, BorderLayout.CENTER);
        } catch (IOException e) {
            MyLogger.log(LogEvent.ERROR, e.getMessage());
            int response = JOptionPane.showConfirmDialog(this,
                    "Sry, couldn't get passive cards for a reason!\n Do you want to try again?",
                    "Passive Cards", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);

            if(response == JOptionPane.YES_OPTION){
                addComponents();
                return;
            }
        }
        add(rootPanel);
    }

    public void setCardClickListener(CardClickListener cardClickListener) {
        cardsPanel.setCardClickListener(cardClickListener);
    }
}
