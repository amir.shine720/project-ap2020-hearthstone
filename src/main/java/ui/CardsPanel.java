package ui;

import cards.Card;
import data.CardProvider;
import data.Player;
import ui.card.CardLabel;
import ui.card.CardType;
import ui.card.PassiveCardLabel;
import ui.inventory.DeckCardLabel;
import ui.inventory.InventoryCardLabel;
import ui.play.PlayCardLabel;
import ui.store.listeners.CardClickListener;
import ui.store.listeners.DeckTransmissionListener;
import ui.store.StoreCardLabel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CardsPanel extends JPanel {
    public static HashMap<CardType, ArrayList<CardLabel>> allCardLabels = new HashMap<>();
    public ArrayList<CardLabel> requiredCrdLabels;
    int cols;

    public static void setAllCards() {
        allCardLabels.clear();
        for (CardType value : CardType.values()) {
            allCardLabels.put(value, new ArrayList<>());
        }
        for (Card card : CardProvider.getAllCards()) {
            for (CardType type : CardType.values()) {
                switch (type) {
                    case STORE:
                        allCardLabels.get(type).add(new StoreCardLabel(card));
                        break;
                    case INVENTORY:
                        allCardLabels.get(type).add(new InventoryCardLabel(card));
                        break;
                    case DECK:
                        allCardLabels.get(type).add(new DeckCardLabel(card));
                        break;
                    case PLAY:
                        allCardLabels.get(type).add(new PlayCardLabel(card));
                        break;
                }
            }
        }
        for (Card passiveCard : CardProvider.getPassiveCards()) {
            allCardLabels.get(CardType.PASSIVE).add(new PassiveCardLabel(passiveCard));
        }
    }

    public CardsPanel(List<Card> cards, CardType type) {
        this(cards, 3, "Cards", type, false);
    }

    public CardsPanel(List<Card> cards, int cols, CardType type) {
        this(cards, cols, "Cards", type, false);
    }


    public CardsPanel(List<Card> cards, int cols, String title, CardType type) {
        this(cards, cols, title, type, false);
    }


    public CardsPanel(List<Card> cards, int cols, String title, CardType type, boolean grayNotOwned) {
        super();

        requiredCrdLabels = new ArrayList<>();

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(title));
        this.cols = cols;

        if (allCardLabels.isEmpty()) {
            setAllCards();
        }

        if (type == CardType.PLAY) {
            setPlayRequiredCard(cards);
        } else {
            setRequiredCards(cards, type, grayNotOwned);
        }

        addComponentsToPanel();
    }

    private void setPlayRequiredCard(List<Card> cards) {
        for (Card card : cards) {
            requiredCrdLabels.add(new PlayCardLabel(card));
        }
    }

    private void setRequiredCards(List<Card> cards, CardType type, boolean grayNotOwned) {
        for (Card card : cards) {
            if (allCardLabels.containsKey(type)) {
                for (CardLabel cardLabel : allCardLabels.get(type)) {
//                    if (Collections.frequency(cards, card) > 1) {
//                        CardsPanel.copyCardLabel(cardLabel);
//                    }

                    if (cardLabel.getCardName().equals(card.getName())) {
                        cardLabel.setCardCount(Collections.frequency(cards, card));
                        if (grayNotOwned && !Player.getInstance().getCards().contains(card)) {
                            cardLabel.setBackground(Color.GRAY);
                        }
                        requiredCrdLabels.add(cardLabel);
                        break;
                    }
                }
            }
        }


    }

    private void addComponentsToPanel() {
        GridBagConstraints gc = new GridBagConstraints();

        gc.weightx = 1;
        gc.weighty = 1;
        gc.ipadx = 50;
        gc.ipady = 50;
        gc.anchor = GridBagConstraints.FIRST_LINE_START;


        int index = 0;
        for (int row = 0; row <= requiredCrdLabels.size() / 3 + 1 || row < 4; row++) {
            for (int col = 0; col < cols; col++) {
                gc.gridx = col;
                gc.gridy = row;
                if (index < requiredCrdLabels.size()) {
                    add(requiredCrdLabels.get(index), gc);
                } else {
                    break;
                }
                index++;
            }
        }
    }

    public void setCardClickListener(CardClickListener cardClickListener) {
        for (CardLabel cardLabel : requiredCrdLabels) {
            cardLabel.setCardClickListener(cardClickListener);
        }
    }

    public void setCardClickListener(DeckTransmissionListener deckTransmissionListener) {
        for (CardLabel cardLabel : requiredCrdLabels) {
            cardLabel.setCardClickListener(deckTransmissionListener);
        }
    }

    public void showButtons() {
        for (CardLabel panel : requiredCrdLabels) {
            panel.showButtons();
        }
    }

    public void hideButtons() {
        for (CardLabel panel : requiredCrdLabels) {
            panel.hideButtons();
        }
    }

}
