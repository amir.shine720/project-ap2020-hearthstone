package ui.card;

public enum CardType {
    STORE,
    INVENTORY,
    PASSIVE,
    DECK,
    PLAY
}
