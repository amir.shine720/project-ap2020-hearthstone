package ui.card;

import cards.Card;

import javax.swing.*;
import java.util.ArrayList;

public class PassiveCardLabel extends CardLabel {
    public PassiveCardLabel(Card card) {
        super(card);
    }

    @Override
    protected void initRequiredComponents(Card card) {
        cardNameLabel = new JLabel(card.getName());
        cardDescriptionLabel = new JTextArea(card.getDescription());
    }

    @Override
    protected void fillSatComponentsIntoArray(ArrayList<JComponent> components) {
        components.add(cardNameLabel);
        components.add(cardDescriptionLabel);
    }
}
