package ui.card;

import cards.Card;
import ui.store.listeners.CardClickListener;
import ui.store.listeners.DeckTransmissionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public abstract class CardLabel extends JLabel {
    DeckTransmissionListener deckTransmissionListener;
    protected Card card;
    GridBagConstraints gc;
    protected JButton addButton;
    protected JButton removeButton;
    protected JTextArea cardDescriptionLabel;
    protected JLabel cardNameLabel;
    protected JLabel cardHeroLabel;
    protected JLabel cardManaLabel;
    protected JLabel cardValueLabel;
    protected JLabel cardHealthLabel;
    protected JLabel cardAttackLabel;
    protected JLabel cardCountLabel;
    protected int cardCount;

    protected ArrayList<JComponent> components;

    public CardLabel(Card card) {
        this(card, 1);
    }

    public CardLabel(Card card, int cardCount) {
        this.cardCount = cardCount;
        this.card = card;
        setLayout(new GridBagLayout());
        setAlignmentX(CENTER_ALIGNMENT);
        setBorder(BorderFactory.createEtchedBorder());
        setPreferredSize(new Dimension(100, 120));

        gc = new GridBagConstraints();
        components = new ArrayList<>();

        initRequiredComponents(card);

        fillSatComponentsIntoArray(components);

        setComponentProperties();

        addComponentsToLayout();
        setOpaque(true);
    }

    private void addComponentsToLayout() {
        if (components.contains(cardManaLabel)) {
            gc.weightx = 2;
            gc.weighty = 1;
            gc.gridx = 0;
            gc.gridy = 0;
            gc.anchor = GridBagConstraints.FIRST_LINE_START;
            add(cardManaLabel, gc);
        }

        if (components.contains(cardNameLabel)) {
            gc.weightx = 5;
            gc.weighty = 1;
            gc.gridx = 1;
            gc.gridy = 0;
            gc.anchor = GridBagConstraints.CENTER;
            add(cardNameLabel, gc);
        }

        if (components.contains(cardCountLabel) /*&& !cardCountLabel.getText().equals("0")*/) {
            gc.weightx = 5;
            gc.weighty = 1;
            gc.gridx = 2;
            gc.gridy = 0;
            gc.anchor = GridBagConstraints.FIRST_LINE_END;
            add(cardCountLabel, gc);
        }

        if (components.contains(cardHeroLabel)) {
            gc.weightx = 5;
            gc.weighty = 1;
            gc.gridx = 1;
            gc.gridy = 1;
            gc.anchor = GridBagConstraints.CENTER;
            add(cardHeroLabel, gc);
        }

        if (components.contains(cardValueLabel)) {
            gc.weightx = 5;
            gc.weighty = 1;
            gc.gridx = 1;
            gc.gridy++;
            gc.anchor = GridBagConstraints.CENTER;
            add(cardValueLabel, gc);
        }

        if (components.contains(cardDescriptionLabel)) {
            gc.weightx = 5;
            gc.weighty = 1;
            gc.gridx = 1;
            gc.gridy++;
            gc.anchor = GridBagConstraints.CENTER;
            add(cardDescriptionLabel, gc);
        }

        if (components.contains(cardHealthLabel) && !cardHealthLabel.getText().equals("0")) {

            gc.gridy++;

            gc.weightx = 2;
            gc.weighty = 1;
            gc.gridx = 0;
            gc.anchor = GridBagConstraints.LAST_LINE_START;
            add(cardHealthLabel, gc);

            gc.weightx = 2;
            gc.weighty = 1;
            gc.gridx = 2;
            gc.anchor = GridBagConstraints.LAST_LINE_END;
            add(cardAttackLabel, gc);
        } else {
            gc.gridy++;
            gc.gridx = 2;
            JLabel emptyLabel = new JLabel("       ");
            add(emptyLabel, gc);
        }

        gc.gridx = 1;
        gc.gridy++;
        gc.anchor = GridBagConstraints.CENTER;

        if (components.contains(addButton)) {
            add(addButton, gc);
        } else if (components.contains(removeButton)) {
            add(removeButton, gc);
        }

    }

    protected abstract void fillSatComponentsIntoArray(ArrayList<JComponent> components);


    private void setComponentProperties() {
        if (cardDescriptionLabel != null) {
            cardDescriptionLabel.setLineWrap(true);
            cardDescriptionLabel.setWrapStyleWord(true);
            cardDescriptionLabel.setOpaque(false);
            cardDescriptionLabel.setEditable(false);
        }
        for (JComponent component : components) {
            component.setFont(new Font("Serif", Font.BOLD, 10));
        }
    }

    protected abstract void initRequiredComponents(Card card);

    public void setCardClickListener(CardClickListener cardClickListener) {

        removePreviousMouseListeners(this);
        removePreviousMouseListeners(cardDescriptionLabel);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                cardClickListener.cardClicked(card);
            }
        });
        cardDescriptionLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                cardClickListener.cardClicked(card);
            }
        });

    }

    protected void removePreviousMouseListeners(JComponent component) {
        for (MouseListener mouseListener : component.getMouseListeners()) {
            component.removeMouseListener(mouseListener);
        }
    }

    protected void removePreviousActionListeners(JButton component) {
        for (ActionListener actionListener : component.getActionListeners()) {
            component.removeActionListener(actionListener);
        }
    }

    public void setCardClickListener(DeckTransmissionListener deckTransmissionListener) {
        this.deckTransmissionListener = deckTransmissionListener;
    }

    public void hideButtons() {
    }

    public void showButtons() {
    }

    public String getCardName() {
        return card.getName();
    }

    public int getCardCount() {
        return cardCount;
    }

    public void setCardCount(int count) {
        if (cardCountLabel != null) {
            cardCountLabel.setText(String.valueOf(count));
        }
    }
}
