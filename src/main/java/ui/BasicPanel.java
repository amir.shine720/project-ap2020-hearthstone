package ui;

import enums.LogEvent;
import enums.Section;
import logger.MyLogger;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class BasicPanel extends JPanel {
    protected JButton exitButton;
    protected NextSectionListener nextSectionListener;

    public BasicPanel(String borderTitle) {
        super();

        Border innerBorder = BorderFactory.createTitledBorder(borderTitle);
        Border outerBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));

        setLayout(new GridBagLayout());

        exitButton = new JButton("Exit");
        exitButton.addActionListener(actionEvent -> {
            MyLogger.log(LogEvent.CLICK_BUTTON, "exit button");
            nextSectionListener.nextSectionRequired(Section.EXIT);
        });
    }

    public void setNextSectionListener(NextSectionListener nextSectionListener) {
        this.nextSectionListener = nextSectionListener;
    }
}
