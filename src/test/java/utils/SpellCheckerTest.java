package utils;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;

public class SpellCheckerTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final InputStream originalIn = System.in;

    @Before
    public void login() throws IOException {
        System.setOut(new PrintStream(outContent));
        TestTools.createAccount();
    }

    @Test
    public void suggestionTestSucceed(){
        Assert.assertEquals("something", SpellChecker.suggest(Arrays.asList("something", "asgard"),"soemthng"));
    }
    @Test
    public void suggestionTestFail(){
        Assert.assertEquals(" ", SpellChecker.suggest(Arrays.asList("asgard", "some invalid"),"soemthng"));
    }

    @After
    public void restoreInOut() {
        TestTools.deleteAccount();
        TestTools.deleteLog();
        System.setOut(originalOut);
        System.setIn(originalIn);
    }
}
