package utils;

import data.CardProvider;
import ui.login.LoginViewModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.Objects;

public class TestTools {
    public static void createAccount() throws IOException {
        CardProvider.initializeCards();
        LoginViewModel loginViewModel = new LoginViewModel();
        loginViewModel.addPlayer("Test", "Test");
    }

    public static void deleteAccount() {
        deleteAccount("Test");
    }

    public static void deleteAccount(String username) {
        File file = new File(String.format("src/main/resources/players/%s.json", username));
        try {
            if (file.exists()) {
                Files.delete(file.toPath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteLog() {
        for (File log : Objects.requireNonNull(new File("src/main/resources/logs").listFiles())) {
            if (log.getName().contains("Test")) {
                try {
                    Files.delete(log.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
