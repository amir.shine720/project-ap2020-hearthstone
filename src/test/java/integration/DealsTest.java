package integration;

import cards.Card;
import data.CardProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ui.inventory.InventoryViewModel;
import ui.login.LoginViewModel;
import ui.store.StoreViewModel;
import utils.SellHeroCardException;
import utils.TestTools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Objects;

public class DealsTest {
    private final PrintStream originalOut = System.out;
    private final InputStream originalIn = System.in;

    @Before
    public void login() throws IOException {
        CardProvider.initializeCards();
        TestTools.createAccount();
    }

    @Test
    public void whenSellCard_shouldRemoveFromCards() throws SellHeroCardException {
        StoreViewModel storeViewModel = new StoreViewModel();
        Card randomCard = storeViewModel.getAvailableCards().get(5);
        storeViewModel.sellCard(randomCard);

        InventoryViewModel inventoryViewModel = new InventoryViewModel();
        Assert.assertFalse(inventoryViewModel.getPlayerCards().contains(randomCard));
    }

    @Test
    public void whenExitAndLogin_shouldAddToPreviousLog() throws IOException {
        LoginViewModel loginViewModel = new LoginViewModel();
        loginViewModel.savePlayerData();

        loginViewModel.loginPlayer("Test", "Test");

        ArrayList<File> files = new ArrayList<>();
        for (File file : Objects.requireNonNull(new File("src/main/resources/logs").listFiles())) {
            if (file.getName().contains("Test")) {
                files.add(file);
            }
        }
        Assert.assertEquals(1, files.size());
    }

    @After
    public void restoreInOut() {
        TestTools.deleteLog();
        TestTools.deleteAccount();
        System.setOut(originalOut);
        System.setIn(originalIn);
    }
}
