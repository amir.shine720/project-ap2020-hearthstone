package logger;

import data.Player;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ui.login.LoginViewModel;
import ui.settings.SettingsViewModel;
import utils.TestTools;

import java.io.*;
import java.nio.file.Files;
import java.util.stream.Collectors;

public class LoggerTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final InputStream originalIn = System.in;
    String playerID;
    String username;

    @Before
    public void login() throws IOException {
        System.setOut(new PrintStream(outContent));
        TestTools.createAccount();
        playerID = Player.getInstance().getID();
        username = Player.getInstance().getUsername();
    }

    @Test
    public void whenUserCreateAccount_addNewLoggerFile() {
        File file = new File(String.format("src/main/resources/logs/Test-%s.log", playerID));
        Assert.assertTrue(file.exists());
    }

    @Test
    public void whenUserCreateAccount_logFileWriteHeader() throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader(String.format("src/main/resources/logs/Test-%s.log", playerID)))) {

            Assert.assertTrue(bufferedReader.readLine().contains("USER: Test") &&
                    bufferedReader.readLine().contains("CREATED_AT:") &&
                    bufferedReader.readLine().contains("PASSWORD:"));
        }
    }

    @Test
    public void whenDeletePlayer_shouldAddDeletedAt() throws IOException {
        SettingsViewModel settingsViewModel = new SettingsViewModel();
        settingsViewModel.deleteAccount("Test");
        try (BufferedReader bufferedReader = new BufferedReader(
                        new FileReader(String.format("src/main/resources/logs/Test-%s.log", playerID)))) {
            Assert.assertTrue(bufferedReader.lines().collect(Collectors.joining()).contains("DELETED_AT:"));
        }
    }

    @Test
    public void whenSignUp_addSignUpLog() throws IOException {

        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader(String.format("src/main/resources/logs/Test-%s.log", playerID)))) {
            Assert.assertTrue(bufferedReader.lines().collect(Collectors.joining()).contains("sign_in"));
        }
    }

    @Test
    public void whenCantOpenPlayerLog_createANewOne() throws IOException {
        String path = String.format("src/main/resources/logs/%s-%s.log",
                username, playerID);
        File file = new File(path);
        Files.delete(file.toPath());
        LoginViewModel loginViewModel = new LoginViewModel();
        loginViewModel.loginPlayer("Test", "Test");
        Assert.assertTrue(new File(path).exists());
    }

    @After
    public void restoreInOut() {
        System.setOut(originalOut);
        TestTools.deleteAccount();
        TestTools.deleteLog();
        System.setIn(originalIn);
    }
}
