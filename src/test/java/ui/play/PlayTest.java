package ui.play;

import cards.Card;
import data.CardProvider;
import data.Player;
import enums.Hero;
import enums.Type;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ui.card.CardType;
import utils.TestTools;

import java.io.IOException;
import java.util.Objects;

public class PlayTest {
    PlayViewModel viewModel;

    @Before
    public void initializeLogin() throws IOException {
        TestTools.createAccount();
        viewModel = new PlayViewModel();
    }

    @Test
    public void whenStartGame_noPlayedCard() {
        Assert.assertTrue(viewModel.getCurrentPlayerPlayedCards().isEmpty());
    }

    @Test
    public void whenPlayCard_addToPlayedCards() {
        viewModel.getCurrentPlayer().addToPlayedCard(new Card());
        viewModel.getOtherPlayer().addToPlayedCard(new Card());
        Assert.assertEquals(1, viewModel.getCurrentPlayerPlayedCards().size());
        Assert.assertEquals(1, viewModel.getOtherPlayerPlayedCards().size());
    }

    @Test
    public void whenStartGame_haveThreeHeldCards() {
        Assert.assertEquals(3, viewModel.getCurrentPlayerHeldCards().size());
        Assert.assertEquals(3, viewModel.getOtherPlayerHeldCards().size());
    }

    @Test
    public void whenTurnFinish_addRandomCard() {
        viewModel.endCurrentPlayerTurn();
        Assert.assertEquals(4, viewModel.getCurrentPlayerHeldCards().size());
    }

    @Test
    public void whenMatchStart_showPassiveCards() throws IOException {
        Assert.assertEquals(Type.PASSIVE, viewModel.getRandomPassiveCards().get(0).getType());
    }

    @Test
    public void whenRequestPlayerMana_getMana()  {
        Assert.assertEquals(1, viewModel.getCurrentPlayerMana());
        Assert.assertEquals(1, viewModel.getOtherPlayerMana());
    }

    @Test
    public void whenTurnEnds_resetMana()  {
        viewModel.setCurrentPlayerMana(0);
        viewModel.endCurrentPlayerTurn();
        Assert.assertEquals(2, viewModel.getCurrentPlayerMana());
    }

    @Test
    public void whenTurnEnds_addManaCap()  {
        viewModel.endCurrentPlayerTurn();
        Assert.assertEquals(2, viewModel.getCurrentPlayerManaCap());
    }

    @Test
    public void whenHeroWarlock_startHealth35()  {
        Player.getInstance().setHero(Hero.WARLOCK);
        viewModel = new PlayViewModel();
        Assert.assertEquals(35, viewModel.getCurrentPlayer().getHealth());
    }

    @Test
    public void whenMoreThan12CardsDrawn_showError()  {
        for (int i = 0; i < 13; i++) {
            viewModel.addRandomCardCurrentPlayerHeld();
        }
        Assert.assertEquals(12, viewModel.getCurrentPlayerHeldCards().size());
    }

    @Test
    public void whenPlayCard_reducePlayerMana()  {
        viewModel.playCurrentPlayerCard(Objects.requireNonNull(CardProvider.getCardByName("Smuggler's Run")));
        Assert.assertEquals(0, viewModel.getCurrentPlayerMana());
    }

    @Test
    public void whenPlayCard_removeFromHeldCards()  {
        Card card = viewModel.getCurrentPlayerHeldCards().get(0);
        viewModel.playCurrentPlayerCard(card);
        Assert.assertFalse(viewModel.getCurrentPlayerHeldCards().contains(card));
    }

    @Test
    public void whenManaCapReached_shouldNotIncrease()  {
        for (int i = 0; i < 11; i++) {
            viewModel.increaseCurrentPlayerManaCap();
        }
        Assert.assertEquals(10, viewModel.getCurrentPlayerManaCap());
    }

    @Test
    public void whenPlaySpellCard_shouldNotAddToPlayedOnes()  {
        viewModel.playCurrentPlayerCard(Objects.requireNonNull(CardProvider.getCardByName("Arcane Intellect")));
        Assert.assertEquals(0, viewModel.getCurrentPlayerPlayedCards().size());
    }

    @After
    public void restoreStreams() {
        TestTools.deleteAccount();
        TestTools.deleteLog();
    }

}
