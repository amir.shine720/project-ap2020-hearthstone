package ui.inventory;

import cards.Card;
import cards.Deck;
import data.CardProvider;
import data.Player;
import enums.Hero;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ui.terminal.TerminalViewModel;
import utils.FullDeckException;
import utils.SameCardLimitException;
import utils.TestTools;

import java.io.IOException;
import java.util.List;

public class InventoryTest {

    InventoryViewModel inventoryViewModel;


    @Before
    public void login() throws IOException {
        CardProvider.initializeCards();
        TestTools.createAccount();
        inventoryViewModel = new InventoryViewModel();
    }

    @Test
    public void whenOpenInventory_shouldAddHeroesInFilters() {
        Assert.assertEquals(Hero.getAllHeroes(), inventoryViewModel.getAllHeroes());
    }

    // We don't need player's hero in Inventory for now, so this test remains commented
//    @Test
//    public void whenRequestDashMHeroes_showCurrentHero() {
//
//        System.setIn(new ByteArrayInputStream("ls -m -heroes\nexit".getBytes()));
//        Inventory.openInventory();
//        Assert.assertTrue(outContent.toString().contains("Mage"));
//    }

    @Test
    public void whenRequestSelectHero_changePlayerHero() {
        inventoryViewModel.changePlayerHero(Hero.WARLOCK);
        Assert.assertEquals(Hero.WARLOCK, Player.getInstance().getHero());

    }

    @Test
    public void whenOpenInventory_showAllCards() {
        TerminalViewModel terminalViewModel = new TerminalViewModel();
        Assert.assertEquals(terminalViewModel.getAllCards(), inventoryViewModel.getAllCards());
    }

    @Test
    public void whenRequestDashMCards_showPlayerDeck() {
        Assert.assertEquals(Player.getInstance().getCurrentDeck(), inventoryViewModel.getPlayerDeck());
    }

    // TODO: this test needs modification
    @Test
    public void ifRequestAddCard_addCardToDeck() throws FullDeckException, SameCardLimitException {
        Card randomCard = inventoryViewModel.getPlayerDeck().getCards().get(0);
        inventoryViewModel.removeCardFromDeck(randomCard);
        inventoryViewModel.addCardToDeck(randomCard);
        Assert.assertTrue(inventoryViewModel.getPlayerDeck().getCards().contains(randomCard));
    }

    @Test
    public void ifRequestRemoveCard_removeCardFromDeck() {
        Card randomCard = Player.getInstance().getCurrentDeck().getCards().get(0);
        InventoryViewModel inventoryViewModel = new InventoryViewModel();
        inventoryViewModel.removeCardFromDeck(randomCard);
        Assert.assertFalse(Player.getInstance().getCurrentDeck().getCards().contains(randomCard));
    }

    // TODO: should rewrite this test after enough cards has been added
//    @Test(expected = FullDeckException.class)
//    public void whenDeckIsFull_showError() throws FullDeckException, SameCardLimitException {
//        Card randomCard = inventoryViewModel.getPlayerDeck().getCards().get(0);
//
//        for (int memberCount = Player.getInstance().getCurrentDeck().getCards().size()
//             ; memberCount < 16; memberCount++) {
//            inventoryViewModel.addCardToDeck(randomCard);
//        }
//    }

    @Test(expected = SameCardLimitException.class)
    public void whenMultipleOfSameCardDeck_showError() throws FullDeckException, SameCardLimitException {
        Card randomCard = inventoryViewModel.getPlayerDeck().getCards().get(0);

        for (int i = 0; i < 2; i++) {
            inventoryViewModel.addCardToDeck(randomCard);
        }
    }


    @Test
    public void ifRequestNotOwned_showNotOwnedCards() {
        Card randomCard = inventoryViewModel.getPlayerDeck().getCards().get(0);
        Assert.assertFalse(inventoryViewModel.getNotOwnedCards().contains(randomCard));
    }

    @Test
    public void ifRequestAddableCardsToDeck_showCardInDeck() {
        Deck deck = inventoryViewModel.getPlayerDeck();
        Card randomCard = inventoryViewModel.getPlayerDeck().getCards().get(0);
        inventoryViewModel.removeCardFromDeck(randomCard);
        deck.getCards().retainAll(inventoryViewModel.getAddableCardsToDeck(deck));
        Assert.assertFalse(deck.getCards().isEmpty());
    }


    @Test
    public void ifRequestPlayersDecks_returnAllPlayersDecks() {
        Assert.assertEquals(Player.getInstance().getDecks(), inventoryViewModel.getAllPlayerDecks());
    }

    @Test
    public void ifRequestSetPlayerCurrentDeck_changePlayerDeck() {
        Deck deck = inventoryViewModel.getAllPlayerDecks().get(2);
        inventoryViewModel.setPlayerCurrentDeck(deck);
        Assert.assertEquals(Player.getInstance().getCurrentDeck(), deck);
    }

    @Test
    public void ifRequestFilteredCardsDefault_getCardsWithFilter() {
        boolean assertion = true;
        for (Card card : inventoryViewModel.getCardsWithFilter()) {
            if (card.getHero() != Hero.NATURAL && !inventoryViewModel.getAllCards().contains(card)) {
                assertion = false;
            }
        }
        Assert.assertTrue(assertion);
    }

    @Test
    public void ifRequestFilteredCardsNotOwned_getCardsWithFilter() {
        CardFilter.getCardFilter().setNotOwned(true);
        CardFilter.getCardFilter().setOwned(false);
        Assert.assertFalse(inventoryViewModel.getPlayerCards().containsAll(inventoryViewModel.getCardsWithFilter()));
        CardFilter.reset();
    }

    @Test
    public void ifRequestFilteredCardsOwned_getCardsWithFilter() {
        CardFilter.getCardFilter().setNotOwned(false);
        CardFilter.getCardFilter().setOwned(true);
        Assert.assertTrue(inventoryViewModel.getPlayerCards().containsAll(inventoryViewModel.getCardsWithFilter()));
        CardFilter.reset();
    }

    @Test
    public void ifRequestFilteredCardsTwoMana_getCardsWithFilter() {
        CardFilter.getCardFilter().setMana(2);
        boolean assertion = true;
        for (Card card : inventoryViewModel.getCardsWithFilter()) {
            if (card.getManaCost() != 2) {
                assertion = false;
                break;
            }
        }
        Assert.assertTrue(assertion);
        CardFilter.reset();

    }

    @Test
    public void ifRequestFilteredName_getCardsWithFilter() {
        CardFilter.getCardFilter().setName("Bug Cre");
        Assert.assertTrue(inventoryViewModel.getCardsWithFilter().get(0).getName().contains("Bug"));
        CardFilter.reset();

    }

    @Test
    public void ifRequestFilteredHero_getCardsWithFilter() {
        CardFilter.getCardFilter().setHero(Hero.ROGUE);
        Assert.assertEquals(Hero.ROGUE, inventoryViewModel.getCardsWithFilter().get(0).getHero());
        CardFilter.reset();

    }

    @Test
    public void ifRequestAddableCardsToDeck_getCardsOutOfCurrentDeck() {
        List<Card> cards = inventoryViewModel.getAddableCardsToDeck();
        Assert.assertEquals(Player.getInstance().getHero(), cards.get(0).getHero());
    }

    @Test
    public void ifRequestCreateNewDeck_createEmptyDeckAndEquip() {
        inventoryViewModel.createEmptyDeck(Hero.ROGUE, "newDeck");
        Assert.assertEquals(0, Player.getInstance().getCurrentDeck().getCards().size());
    }

    @Test
    public void ifRequestDeleteDeck_deleteCurrentDeck() {
        Deck deck = Player.getInstance().getCurrentDeck();
        inventoryViewModel.deleteDeck(deck);
        Assert.assertFalse(Player.getInstance().getDecks().contains(deck));
    }

    @Test
    public void ifRequestRenameDeck_renameCurrentDeck() {
        String newName = "anotherName";
        inventoryViewModel.renameCurrentDeck(newName);
        Assert.assertEquals(newName, Player.getInstance().getCurrentDeck().getName());
    }

    @Test
    public void ifRequestSetPlayerHero_changePlayerHero() {
        Hero hero = Hero.ROGUE;
        inventoryViewModel.setPlayerHero(hero);
        Assert.assertEquals(hero, Player.getInstance().getHero());
    }


    @After
    public void restoreInOut() {
        TestTools.deleteAccount();
        TestTools.deleteLog();
    }
}
