package ui.store;

import cards.Card;
import data.CardProvider;
import data.Player;
import enums.Hero;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import utils.SellHeroCardException;
import utils.TestTools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Objects;

public class StoreTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final InputStream originalIn = System.in;
    StoreViewModel storeViewModel;

    @Before
    public void login() throws IOException {
        System.setOut(new PrintStream(outContent));
        TestTools.createAccount();
        storeViewModel = new StoreViewModel();
    }

    @Test
    public void whenRequestBuy_purchaseCardForPlayer() {
        Player.getInstance().setCoins(200);
        Card randomCard = storeViewModel.getAvailableCards().get(0);
        storeViewModel.purchaseCard(randomCard);
        Assert.assertTrue(Player.getInstance().getCards().contains(randomCard));
    }


    @Test
    public void whenRequestWallet_showPlayerCoins() {
        Assert.assertEquals(Player.getInstance().getCoins(), storeViewModel.getPlayerCoins());
    }

    @Test
    public void whenRequestSell_sellPlayerCard() throws SellHeroCardException {
        Card randomCard = storeViewModel.getSellableCards().get(0);
        storeViewModel.sellCard(randomCard);
        Assert.assertFalse(storeViewModel.getSellableCards().contains(randomCard));

    }


    @Test
    public void whenRequestDashS_showSellableCards() {
        boolean assertion = true;
        for (Card sellableCard : storeViewModel.getSellableCards()) {
            if (sellableCard.getHero() != Hero.NATURAL || storeViewModel.getAvailableCards().contains(sellableCard)) {
                assertion = false;
                break;
            }
        }
        Assert.assertTrue(assertion);
    }

    @Test
    public void whenRequestDashB_showPurchasableCards() {
        boolean assertion = true;
        for (Card availableCard : storeViewModel.getAvailableCards()) {
            if (Player.getInstance().getCards().contains(availableCard)) {
                assertion = false;
            }
        }
        Assert.assertTrue(assertion);
    }

    @Test
    public void whenSellCard_cardShouldAddToPurchasable() throws SellHeroCardException {
        Card randomCard = storeViewModel.getSellableCards().get(0);
        storeViewModel.sellCard(randomCard);
        Assert.assertTrue(storeViewModel.getAvailableCards().contains(randomCard));
    }

    @Test
    public void whenBuyCard_shouldDecreaseCoins() {
        Player.getInstance().setCoins(200);
        int currentMoney = Player.getInstance().getCoins();

        Card randomCard = storeViewModel.getAvailableCards().get(0);
        storeViewModel.purchaseCard(randomCard);

        Assert.assertEquals(currentMoney - randomCard.getValue(), Player.getInstance().getCoins());
    }

    @Test
    public void whenSellCard_shouldIncreaseCoins() throws SellHeroCardException {

        Card randomCard = storeViewModel.getSellableCards().get(0);
        int currentMoney = Player.getInstance().getCoins();
        storeViewModel.sellCard(randomCard);

        Assert.assertEquals(currentMoney + (int) (randomCard.getValue() * 0.75), Player.getInstance().getCoins());
    }

    @Test
    public void whenNoEnoughMoney_showError() {
        Card randomCard = storeViewModel.getAvailableCards().get(0);
        Assert.assertFalse(storeViewModel.purchaseCard(randomCard));
    }

    @Test(expected = SellHeroCardException.class)
    public void whenSellHeroCard_showError() throws SellHeroCardException{
        storeViewModel.sellCard(Objects.requireNonNull(CardProvider.getCardByName("Arcane Intellect")));
    }

    @After
    public void restoreInOut() {
        TestTools.deleteAccount();
        TestTools.deleteLog();
        System.setOut(originalOut);
        System.setIn(originalIn);
    }
}
