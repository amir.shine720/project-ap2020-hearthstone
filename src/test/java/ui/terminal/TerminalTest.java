package ui.terminal;

import cards.Card;
import data.CardProvider;
import data.Player;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import utils.TestTools;

import java.io.*;
import java.nio.file.Files;

public class TerminalTest {

    TerminalViewModel terminalViewModel;

    @Before
    public void login() throws IOException {
        CardProvider.initializeCards();
        TestTools.createAccount();
        terminalViewModel = new TerminalViewModel();
    }


    @Test
    public void whenPlayerRegisters_shouldGetDefaultCards() {
        Assert.assertTrue(Player.getInstance().getCards().containsAll(
                CardProvider.getDefaultCards()));
    }


    @Test(expected = IOException.class)
    public void whenCantSaveData_showWarning() throws IOException {
        File file = new File(String.format("src/main/resources/players/%s.json", Player.getInstance().getUsername()));
        Files.delete(file.toPath());
        file.mkdirs();
        terminalViewModel.save();
    }


    @Test
    public void cardComparisonNull(){
        Card card1 = null;
        Assert.assertNotEquals(new Card(), card1);
    }
    @Test
    public void cardComparisonOtherObjects(){
        Player player = null;
        Assert.assertNotEquals(new Card(), player);
    }

    @After
    public void restoreInOut() {
        TestTools.deleteAccount();
        TestTools.deleteLog();
    }
}
