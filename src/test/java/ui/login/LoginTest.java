package ui.login;

import data.Player;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ui.terminal.TerminalViewModel;
import utils.TestTools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;

public class LoginTest {
    LoginViewModel viewModel;

    @Before
    public void initializeLogin() throws IOException {
        TestTools.createAccount();
        viewModel = new LoginViewModel();
    }

    @Test
    public void ifNotHaveAccount_createAccount() throws IOException {
        Assert.assertTrue(viewModel.addPlayer("Test2", "Test"));
        TestTools.deleteAccount("Test2");

    }

    @Test
    public void ifHaveAccount_login() {
        Assert.assertTrue(viewModel.loginPlayer("Test", "Test"));
    }


    @Test
    public void ifPasswordIncorrect_tryAgain() {
        Assert.assertFalse(viewModel.loginPlayer("Test", "Invalid"));

    }

    @Test
    public void ifUsernameInvalid_tryAgain() {
        Assert.assertFalse(viewModel.loginPlayer("Invalid", "Test"));
    }

    @Test
    public void ifUsernameExists_usernameExistsTryAgain() throws IOException {
        Assert.assertFalse(viewModel.addPlayer("Test", "Test"));
    }

    @Test
    public void ifUsernameExistsLowercase_usernameExists() throws IOException {
        Assert.assertFalse(viewModel.addPlayer("test", "Test"));
    }

    @Test
    public void whenExistUsername_noDefaultCardSet() throws IOException {
        TerminalViewModel terminalViewModel = new TerminalViewModel();
        terminalViewModel.logOut();
        viewModel.addPlayer("Test", "Test");
        viewModel.addPlayer("Test2", "Test2");
        Assert.assertEquals(Player.getInstance().getCurrentHeroCards().size(),
                new HashSet<>(Player.getInstance().getCurrentHeroCards()).size());
        TestTools.deleteAccount("Test2");
    }

    @Test
    public void ifNoPlayerSet_noSaveData() throws IOException {
        Player.getInstance().clear();
        viewModel.savePlayerData();
        File file = new File("src/main/resources/players/null.json");
        Assert.assertFalse(file.exists());
    }

    @After
    public void restoreStreams() {
        TestTools.deleteAccount();
        TestTools.deleteLog();
    }
}
