package ui.settings;

import data.Player;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import utils.TestTools;

import java.io.IOException;

public class SettingsTest {
    SettingsViewModel viewModel;

    @Before
    public void initializeLogin() throws IOException {
        TestTools.createAccount();
        viewModel = new SettingsViewModel();
    }


    @Test
    public void ifRequestDeletePlayer_deleteCurrentPlayer() throws IOException {
        viewModel.deleteAccount("Test");
        Assert.assertNull(Player.getInstance().getUsername());
    }

    @Test
    public void ifRequestDeletePlayerWrongPassword_returnError() throws IOException {
        Assert.assertFalse(viewModel.deleteAccount("Incorrect Password"));
    }

    @After
    public void restoreStreams() {
        TestTools.deleteAccount();
        TestTools.deleteLog();
    }
}
